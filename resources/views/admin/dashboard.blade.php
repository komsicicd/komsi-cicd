@extends('layouts.dashboard')
@extends('admin.menu')

@section('title', 'KEJURLAT 2019 | Dashboard')
@section('menus')
    <li class="active">
        <a href="{{ route('dashboard.admin') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('verify.admin') }}"><i class="fa fa-tasks"></i> <span class="nav-label">Kelompok Proyek</span></a>
    </li>
    <li>
        <a href="{{ route('gitlab.admin') }}"><i class="fab fa-gitlab"></i> <span class="nav-label">Gitlab & Chat Room</span></a>
    </li>
    <li>
        <a href="{{ route('docker.admin') }}"><i class="fab fa-docker"></i> <span class="nav-label">Konfigurasi Docker</span></a>
    </li>
    <li>
        <a href="{{ route('info.admin') }}"><i class="fa fa-bullhorn"></i> <span class="nav-label">Pemberitahuan</span></a>
    </li>
@stop
@section('content')
<div class="wrapper wrapper-content">
   <div class="row">
               <div class="col-lg-3">
                   <div class="ibox float-e-margins">
                       <div class="ibox-title">
                           <span class="label label-success pull-right"><i class="fa fa-clock-o"> {{ date('d.m.Y') }}</i></span>
                           <h5>Kelompok Proyek</h5>
                       </div>
                       <div class="ibox-content">
                           <h1 class="no-margins">{{$tkel}} Kelompok</h1>
                           <!-- <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div> -->
                           <small>Proyek Akhir</small>
                       </div>
                   </div>
               </div>
               <div class="col-lg-3">
                   <div class="ibox float-e-margins">
                       <div class="ibox-title">
                           <span class="label label-info pull-right"><i class="fa fa-clock-o"> {{ date('d.m.Y') }}</i></span>
                           <h5>Anggota</h5>
                       </div>
                       <div class="ibox-content">
                           <h1 class="no-margins">{{$tang}} Anggota</h1>
                           <!-- <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div> -->
                           <small>dari {{$tkel}} kelompok</small>
                       </div>
                   </div>
               </div>
               <div class="col-lg-3">
                   <div class="ibox float-e-margins">
                       <div class="ibox-title">
                           <span class="label label-primary pull-right"><i class="fa fa-clock-o"> {{ date('d.m.Y') }}</i></span>
                           <h5>Repository</h5>
                       </div>
                       <div class="ibox-content">
                           <h1 class="no-margins">{{ $trep }}</h1>
                           <!-- <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div> -->
                           <small>Total Repository</small>
                       </div>
                   </div>
               </div>
               <div class="col-lg-3">
                   <div class="ibox float-e-margins">
                       <div class="ibox-title">
                           <span class="label label-danger pull-right"><i class="fa fa-clock-o"> {{ date('d.m.Y') }}</i></span>
                           <h5>Docker Image</h5>
                       </div>
                       <div class="ibox-content">
                           <h1 class="no-margins">{{ $tdoc }}</h1>
                           <!-- <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div> -->
                           <small>Total Docker Image</small>
                       </div>
                   </div>
               </div>
     </div>

     <div class="row">
         <div class="col-lg-12">
             <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div>
                                    <span class="pull-right text-right">
                                    <small>Total kelompok proyek terdaftar dan telah selesai <strong>di Konfigurasi</strong></small>
                                        <br>
                                        Tanggal 20 Mei 2020
                                    </span>
                        <h3 class="font-bold no-margins">
                            Kelompok Proyek
                        </h3>
                        <small>Mahasiswa KOMSI & TPRL.</small>
                    </div>

                    <div class="m-t-sm">

                        <div class="row">
                            <div class="col-md-8">
                                <div>
                                    <canvas id="lineChart" height="402" width="1062" style="width: 531px; height: 201px;"></canvas>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <ul class="stat-list m-t-lg">
                                    <li>
                                        <h2 class="no-margins">10</h2>
                                        <small>Total Proyek terdaftar</small>
                                        <div class="progress progress-mini">
                                            <div class="progress-bar" style="width: 48%;"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <h2 class="no-margins ">4</h2>
                                        <small>Total proyek selesai di konfigurasi</small>
                                        <div class="progress progress-mini">
                                            <div class="progress-bar" style="width: 60%;"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="m-t-md">
                        <small class="pull-right">
                            <i class="fa fa-clock-o"> </i>
                            Update on 16.07.2015
                        </small>
                        <small>
                            <strong>Analysis of sales:</strong> The value has been changed over time, and last month reached a level over $50,000.
                        </small>
                    </div>

                </div>
             </div>
         </div>
     </div>

     <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Timeline</h5>
                    <span class="label label-primary">KEJURLAT 2019</span>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content inspinia-timeline">

                    <div class="timeline-item">
                        <div class="row">
                            <div class="col-xs-3 date">
                                <i class="fa fa-briefcase"></i>
                                {{ date('d-m-Y') }}
                                <br/>
                                <small class="text-navy">Hari Senin</small>
                            </div>
                            <div class="col-xs-7 content no-top-border">
                                <p class="m-b-xs"><strong>Technical Meeting</strong></p>
                                <p>Technical meeting akan dilaksanakan di gedung Grha Sabha Pramana Universitas Gadjah Mada.</p>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="row">
                            <div class="col-xs-3 date">
                                <i class="fa fa-file-text"></i>
                               {{ date('d-m-Y') }}
                                <br/>
                                <small class="text-navy">Hari Selasa</small>
                            </div>
                            <div class="col-xs-7 content">
                                <p class="m-b-xs"><strong>Pengumpulan Berkas</strong></p>
                                <p>Pengumpulan Surat Delagasi dan surat pernyataan di di Gelanggang Mahasiswa Universitas Gadjah Mada.</p>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="row">
                            <div class="col-xs-3 date">
                                <i class="fa fa-coffee"></i>
                                {{ date('d-m-Y') }}
                                <br/>
                                <small class="text-navy">Hari Rabu</small>
                            </div>
                            <div class="col-xs-7 content">
                                <p class="m-b-xs"><strong>Briefing Acara</strong></p>
                                <p>
                                    Panitia Melakukan briefing untuk mempersiapkan alat alat dan akomodasi untuk acara.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="row">
                            <div class="col-xs-3 date">
                                <i class="fa fa-phone"></i>
                                {{ date('d-m-Y') }}
                                <br/>
                                <small class="text-navy">Hari Minggu</small>
                            </div>
                            <div class="col-xs-7 content">
                                <p class="m-b-xs"><strong>Hari PERTANDINGAN</strong></p>
                                <p>
                                    Hari PERTANDINGAN.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
           <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Aktivitas Terkini</h5>
                    <div class="ibox-tools">
                       <a class="collapse-link">
                           <i class="fa fa-chevron-up"></i>
                       </a>
                       <a class="close-link">
                           <i class="fa fa-times"></i>
                       </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-hover no-margins">
                       <thead>
                       <tr>
                           <th width=20%>Updated</th>
                           <th width=40%>Nama Proyek</th>
                           <th width=20%>Nama Ketua</th>
                           <th width=20%>Status</th>
                       </tr>
                       </thead>
                       <tbody>
                        @foreach($notif as $not)
                          <tr>
                              <td><i class="fab fa-clock-o"></i> {{ date('d-m-Y', strtotime($not->updated_at)) }}</td>
                              <td class="text-success"> <i class="fa fa-task"></i> {{ $not->user->nama_proyek }} </td>
                              <td class="text-navy"><i class="fa fa-user"></i> {{ $not->user->nama_ketua }}</td>
                              <td><span class="label label-{{ $not->label }}">{{ $not->status }}</span></td>
                          </tr>
                        @endforeach
                       </tbody>
                    </table>
                </div>
           </div>
        </div>
     </div>
</div>
@stop
@push('scripts')

<!-- ChartJS-->
<script src="{{ asset('master/js/plugins/chartJs/Chart.min.js') }}"></script>
<!-- Flot -->
<script src="{{ asset('master/js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('master/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('master/js/plugins/flot/jquery.flot.spline.js') }}"></script>
<script src="{{ asset('master/js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('master/js/plugins/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('master/js/plugins/flot/jquery.flot.symbol.js') }}"></script>
<script src="{{ asset('master/js/plugins/flot/curvedLines.js') }}"></script>
<script>
    $(document).ready(function() {
        var lineData = {
            labels: ["Januari", "Februari", "Maret", "April", "Mei", "Jun", "Juli"],
            datasets: [
                {
                    label: "Example dataset",
                    fillColor: "rgba(220,220,220,0.5)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [65, 59, 40, 51, 36, 25, 40]
                },
                {
                    label: "Example dataset",
                    fillColor: "rgba(26,179,148,0.5)",
                    strokeColor: "rgba(26,179,148,0.7)",
                    pointColor: "rgba(26,179,148,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [48, 48, 60, 39, 56, 37, 30]
                }
            ]
        };

        var lineOptions = {
            scaleShowGridLines: true,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            bezierCurve: true,
            bezierCurveTension: 0.4,
            pointDot: true,
            pointDotRadius: 4,
            pointDotStrokeWidth: 1,
            pointHitDetectionRadius: 20,
            datasetStroke: true,
            datasetStrokeWidth: 2,
            datasetFill: true,
            responsive: true,
        };


        var ctx = document.getElementById("lineChart").getContext("2d");
        var myNewChart = new Chart(ctx).Line(lineData, lineOptions);
    });
</script>
@endpush


