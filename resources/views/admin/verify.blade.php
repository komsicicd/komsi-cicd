@extends('layouts.dashboard')
@extends('admin.menu')

@section('title', 'Konfirmasi Pendaftaran Proyek | Dashboard Admin')
@push('styles')
<meta name="csrf" content="{{ csrf_token() }}">
<link href="{{asset('master/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">
<link href="{{asset('master/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{ asset('master/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
@endpush
@section('menus')
    <li>
        <a href="{{ route('dashboard.admin') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
    </li>
    <li class="active">
        <a href="{{ route('verify.admin') }}"><i class="fa fa-tasks"></i> <span class="nav-label">Kelompok Proyek</span></a>
    </li>
    <li>
        <a href="{{ route('gitlab.admin') }}"><i class="fab fa-gitlab"></i> <span class="nav-label">Gitlab & Chat Room</span></a>
    </li>
    <li>
        <a href="{{ route('docker.admin') }}"><i class="fab fa-docker"></i> <span class="nav-label">Konfigurasi Docker</span></a>
    </li>
    <li>
        <a href="{{ route('info.admin') }}"><i class="fa fa-bullhorn"></i> <span class="nav-label">Pemberitahuan</span></a>
    </li>
@stop
@section('content')
      <!-- breadcrumb -->
      <div class="row wrapper border-bottom white-bg page-heading">
         <div class="col-lg-8">
              <h2>Konfirmasi Pendaftaran Proyek</h2>
              <ol class="breadcrumb">
                 <li>
                     <a href="{{ route('dashboard.admin') }}">Dashboard</a>
                 </li>
                 <li class="active">
                     <strong>Konfirmasi Pendaftaran Proyek</strong>
                 </li>
              </ol>
         </div>
         <div class="col-lg-4">
              <div class="title-action animated fadeInRight">
                  <a href="#" class="btn btn-primary toggle_atlit" data-toggle="modal" data-target="#tambah_anggota" style="display: {{ Auth::user()->progress >= 15 ? 'none' : '' }}"><i class="fa fa-plus"></i> Proyek Baru </a>
              </div>
         </div>
      </div>
         <!-- end breadcrumb -->

         <!-- EDIT Proyek -->
         <div class="modal inmodal" id="editProyek" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3>Edit Proyek</h3>
                    </div>
                    <div class="modal-body">
                        <form class="m-t" role="form" method="POST" action="{{ route('edit.proyek.admin') }}">
                        @csrf
                        <input type="hidden" name="id" id="aidi">
                        <div class="form-group">
                            <select id="edProgress" class="form-control" name="progress">
                                <option value="0">Verified</option>
                                <option value="15">Confirm Gitlab</option>
                                <option value="30">Docker/Android</option>
                                <option value="45">Docker Config</option>
                                <option value="60">Confirm Docker</option>
                                <option value="80">Done</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input placeholder="Nama Proyek" id="edNama_proyek" type="text" class="form-control @error('nama_proyek') is-invalid @enderror" name="nama_proyek" value="{{ old('nama_proyek') }}" required autocomplete="Nama Proyek" autofocus>

                            @error('nama_proyek')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Ketua Kelompok" id="edNama_ketua" type="text" class="form-control @error('nama_ketua') is-invalid @enderror" name="nama_ketua" value="{{ old('nama_ketua') }}" required autocomplete="Nama Ketua" autofocus>

                            @error('nama_ketua')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Pembimbing Proyek" id="edNama_pembimbing" type="text" class="form-control @error('nama_pembimbing') is-invalid @enderror" name="nama_pembimbing" value="{{ old('nama_pembimbing') }}" required autocomplete="Nama Pembimbing" autofocus>

                            @error('nama_pembimbing')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Email" id="edEmail" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="Email">

                            @error('email')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon">+62</span>
                            <input placeholder="No WhatsApp" id="edNo_telp" type="number" class="form-control @error('no_telp') has-error @enderror" name="no_telp" value="{{ old('no_telp') }}" required autocomplete="No Telp">

                            @error('no_telp')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Password" id="edPassword" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                  </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                           <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                     </form>
                </div>
            </div>
         </div>
         <!-- END EDIT -->

      <!-- Tambah Proyek -->
      <div class="modal inmodal" id="tambah_anggota" tabindex="-1" role="dialog"  aria-hidden="true">
         <div class="modal-dialog">
             <div class="modal-content animated fadeIn">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                     <h3>Tambah Proyek</h3>
                  </div>
                  <div class="modal-body">
                    <form class="m-t" role="form" method="POST" action="{{ route('add.proyek.admin') }}">
                        @csrf
                        <div class="form-group">
                            <input placeholder="Nama Proyek" id="nama_proyek" type="text" class="form-control @error('nama_proyek') is-invalid @enderror" name="nama_proyek" value="{{ old('nama_proyek') }}" required autocomplete="nama_proyek" autofocus>

                            @error('nama_proyek')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Ketua Kelompok" id="nama_ketua" type="text" class="form-control @error('nama_ketua') is-invalid @enderror" name="nama_ketua" value="{{ old('nama_ketua') }}" required autocomplete="nama_ketua" autofocus>

                            @error('nama_ketua')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Pembimbing Proyek" id="nama_pembimbing" type="text" class="form-control @error('nama_ketua') is-invalid @enderror" name="nama_pembimbing" value="{{ old('nama_pembimbing') }}" required autocomplete="nama_pembimbing" autofocus>

                            @error('nama_pembimbing')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon">+62</span>
                            <input placeholder="No WhatsApp" id="no_telp" type="number" class="form-control @error('no_telp') has-error @enderror" name="no_telp" value="{{ old('no_telp') }}" required autocomplete="no_telp">

                            @error('no_telp')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback text-danger text-left" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Komfirmasi Password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                  </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                           <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                     </form>
             </div>
         </div>
      </div>
         <!-- End Modal -->


         <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Data Pendaftaran Proyek<sup class="text-danger">*</sup></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover" id="ppTable" >
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Proyek</th>
                                <th>Ketua Kelompok</th>
                                <th>Pembimbing Proyek</th>
                                <th>E-mail Ketua</th>
                                <th>No. WhatsApp</th>
                                <th>Default Password</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>

                    </div>
                    </div>
         </div>
         <div class="col-lg-12">
            <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Data Proyek Terdaftar<sup class="text-danger">*</sup></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
            <table class="table table-striped table-bordered table-hover" id="ptTable" >
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Proyek</th>
                        <th>Ketua Kelompok</th>
                        <th>Pembimbing Proyek</th>
                        <th>E-mail Ketua</th>
                        <th>No. WhatsApp</th>
                        <th>Default Password</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <div id="MessageContainer"></div>
            </table>

            </div>
            </div>
        </div>
      </div>

      <div class="row">
        <!-- Info Panel -->
          <div class="info2">
            <div class="col-sm-3 pull-left">
                <div class="panel panel-info">
                    <div class="panel-heading">
                         <i class="fa fa-info-circle"></i> Info
                    </div>
                    <div class="panel-body">
                       <div class="danger"><i class="fa fa-circle text-danger"></i> <span><sup class="text-danger">* </sup>Wajib diisi</span></div>
                       <div class="danger"><i class="fa fa-circle text-warning"></i> <span>Pastikan kelengkapan data sebelum kunci data!</span></div>
                       <div class="danger"><i class="fa fa-circle text-warning"></i> <span>Jika terdapat kesalahan input atau pengajuan tambah data,</span></div>
                       <div class="warning"><i class="fa fa-circle text-white"></i> <span>silahkan isi form di tab pengajuan</span></div>
                       <div class="warning"><i class="fa fa-circle text-info"></i> <span>Ubah Password gitlab setelah pertama kali login!</span></div>
                       <div class="warning"><i class="fa fa-circle text-info"></i> <span>Credential Gitlab dapat dipakai setelah di konfirmasi oleh Admin</span></div>
                    </div>
                </div>
            </div>
          </div>
      </div>

    </div>

{{-- @endif --}}
@stop

@push('scripts')
@if(Session::has('anggota'))
<script>
   $(document).ready(function() {
       toastr.options = {
           closeButton: true,
           progressBar: true,
           preventDuplicates: true,
           positionClass: 'toast-top-right',
       };
       toastr.success('{{ Session::get('anggota') }}', 'Berhasil!');

   });
</script>
@endif
<!-- Jquery Validate -->
<script src="{{ asset('master/js/plugins/validate/jquery.validate.min.js') }}"></script>

<!-- SWAL -->
<script src="{{ asset('master/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script>
    $(document).ready(function(){
    //Confirm Proyek
    $(document).on('click', '.cProyek', function () {
         var ang_id = $(this).data('id');
          swal({
                      title: "Anda Yakin?",
                      text: "Proyek ini akan dikonfirmasi dan user dapat login ke Dashboard",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Ya, Konfirmasi!",
                      cancelButtonText: "Tidak!",
                      closeOnConfirm: false,
                      closeOnCancel: false },
                  function (isConfirm) {
                      if (isConfirm) {
                           $.ajax({
                              url: "{{ route('confirm.proyek.admin') }}",
                              method: "POST",
                              headers: {
                                 "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                              },
                              data: { id: ang_id },
                              success: function(){
                                 swal("Dikonfirmasi!", "Proyek berhasi dikonfirmasi!.", "success");
                                 setTimeout(function(){
                                    window.location.reload();
                                 }, 1500);
                              },
                              error: function(){
                                 swal("ERROR", "Terjadi kesalahan pada saat mengkonfirmasi proyek!", "error");
                              }
                           });
                      } else {
                          swal("Dibatalkan", "Proyek tidak di konfirmasi", "error");
                      }
                  });
            });
        // END CONFIRM

    //Batal Confirm Proyek
    $(document).on('click', '.ucProyek', function () {
         var ang_id = $(this).data('id');
          swal({
                      title: "Anda Yakin?",
                      text: "Proyek ini akan bata dikonfirmasi dan user tidak dapat login ke Dashboard",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Ya, Batal Konfirmasi!",
                      cancelButtonText: "Tidak!",
                      closeOnConfirm: false,
                      closeOnCancel: false },
                  function (isConfirm) {
                      if (isConfirm) {
                           $.ajax({
                              url: "{{ route('unconfirm.proyek.admin') }}",
                              method: "POST",
                              headers: {
                                 "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                              },
                              data: { id: ang_id },
                              success: function(){
                                 swal("Dibatalkan!", "Proyek berhasi dibatalkan!.", "success");
                                 setTimeout(function(){
                                    window.location.reload();
                                 }, 1500);
                              },
                              error: function(){
                                 swal("ERROR", "Terjadi kesalahan pada saat membatalkan proyek!", "error");
                              }
                           });
                      } else {
                          swal("Dibatalkan", "Proyek tidak dibatalkan", "error");
                      }
                  });
            });
        // END CONFIRM

    //Edit Proyek
    $(document).on('click', '.editProyek', function () {
         var ang_id = $(this).data('id');
               $.ajax({
                  url: "{{ route('dataEdit.proyek.admin') }}",
                  method: "POST",
                  headers: {
                     "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                  },
                  data: { id: ang_id },
                  success: function(data){
                      console.log(data);
                     $('#aidi').val(ang_id);
                     $('#edNama_proyek').val(data[0].nama_proyek);
                     $('#edNama_ketua').val(data[0].nama_ketua);
                     $('#edNama_pembimbing').val(data[0].nama_pembimbing);
                     $('#edEmail').val(data[0].email);
                     $('#edNo_telp').val(data[0].no_telp);
                  },
                  error: function(){
                     swal("ERROR", "Terjadi kesalahan pada saat menghapus data!", "error");
                  }
               });
      });
      //END EDIT

        // GET DATA
            $('#ppTable').DataTable({
                columnDefs: [{
                   defaultContent: "-",
                   targets: "_all"
                }],
                // dom: '<"html5buttons"B>lTfgitp',
                // buttons: [
                //     {extend: 'copy'},
                //     {extend: 'csv'},
                //     {extend: 'excel', title: 'ExampleFile'},
                //     {extend: 'pdf', title: 'ExampleFile'},

                //     {extend: 'print',
                //      customize: function (win){
                //             $(win.document.body).addClass('white-bg');
                //             $(win.document.body).css('font-size', '10px');

                //             $(win.document.body).find('table')
                //                     .addClass('compact')
                //                     .css('font-size', 'inherit');
                //     }
                //     }
                // ],
                responsive: true,
                prossessing: true,
                serverSide: true,
                ajax: '{!! route('data.pp.admin') !!}',
                columns: [
                  { name: '', data: 'DT_RowIndex' },
                  { name: 'nama_proyek', data: 'nama_proyek' },
                  { name: 'nama_ketua', data: 'nama_ketua' },
                  { name: 'nama_pembimbing', data: 'nama_pembimbing' },
                  { name: 'email', data: 'email' },
                  { name: 'no_telp', data: 'no_telp' },
                  { name: 'pass', data: 'pass' },
                  { name: 'progress', data: 'progress', sortable: false },
                  {
                     name: 'action',
                     data: 'action',
                     sortable: false
                  },
               ]
            });

        $('#ptTable')
            .on( 'draw.dt', function () {
                console.log( 'Loading' );
            } )
            .on( 'init.dt', function () {
                console.log( 'Loaded' );
                $("#MessageContainer").html("")
            } )
            .DataTable({
                columnDefs: [{
                   defaultContent: "-",
                   targets: "_all"
                }],
                responsive: true,
                prossessing: true,
                serverSide: true,
                ajax: '{!! route('data.pt.admin') !!}',
                columns: [
                  { name: '', data: 'DT_RowIndex' },
                  { name: 'nama_proyek', data: 'nama_proyek' },
                  { name: 'nama_ketua', data: 'nama_ketua' },
                  { name: 'nama_pembimbing', data: 'nama_pembimbing' },
                  { name: 'email', data: 'email' },
                  { name: 'no_telp', data: 'no_telp' },
                  { name: 'pass', data: 'pass' },
                  { name: 'progress', data: 'progress', sortable: false },
                  {
                     name: 'action',
                     data: 'action',
                     sortable: false
                  },
               ]
            });
        });
</script>
@endpush
