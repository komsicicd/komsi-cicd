@extends('layouts.dashboard')
@extends('admin.menu')

@section('title', 'KEJURLAT 2019 | Dashboard User')
@push('styles')
<meta name="csrf" content="{{ csrf_token() }}">
<link href="{{asset('master/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">
<link href="{{asset('master/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{ asset('master/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
@endpush
@section('menus')
    <li>
        <a href="{{ route('dashboard.admin') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('verify.admin') }}"><i class="fa fa-tasks"></i> <span class="nav-label">Kelompok Proyek</span></a>
    </li>
    <li>
        <a href="{{ route('gitlab.admin') }}"><i class="fab fa-gitlab"></i> <span class="nav-label">Gitlab & Chat Room</span></a>
    </li>
    <li class="active">
        <a href="{{ route('docker.admin') }}"><i class="fab fa-docker"></i> <span class="nav-label">Konfigurasi Docker</span></a>
    </li>
    <li>
        <a href="{{ route('info.admin') }}"><i class="fa fa-bullhorn"></i> <span class="nav-label">Pemberitahuan</span></a>
    </li>
@stop
@section('content')
      <!-- breadcrumb -->
      <div class="row wrapper border-bottom white-bg page-heading">
         <div class="col-lg-8">
              <h2>Konfirmasi Docker Image</h2>
              <ol class="breadcrumb">
                 <li>
                     <a href="{{ route('dashboard.user') }}">Dashboard</a>
                 </li>
                 <li class="active">
                     <strong>Konfirmasi Docker Image</strong>
                 </li>
              </ol>
         </div>
      </div>
         <!-- end breadcrumb -->
         <!-- Modal -->
      <div class="modal inmodal" id="commentDocker" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
               <div class="modal-content animated fadeIn">
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <h3>Komentar/Revisi</h3>
                   </div>
                   <div class="modal-body">
                     <form method="POST" action="{{ route('komentar.docker.admin') }}" class="">
                         @csrf
                        <input type="hidden" name="user_id" id="idKomentarDocker">
                        <input type="hidden" name="is_admin" value=1>
                        <div class="form-group"><label>Pesan</label> <textarea rows="5" placeholder="Tulis Komentar/Revisi disini..." class="form-control" name="komentar"></textarea></div>
                   </div>
                         <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Kirim</button>
                         </div>
                      </form>
              </div>
            </div>
        </div>
     </div>
         <!-- End Modal -->
        <!-- List Docker Image -->
      <div class="modal inmodal" id="detailDocker" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
               <div class="modal-content animated fadeIn">
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <h3>Link Docker Image</h3>
                   </div>
                   <div class="modal-body">
                    <table class="table" id="detailTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Repository</th>
                                <th>Docker Image</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                   </div>
              </div>
            </div>
        </div>
     </div>
         <!-- End Modal -->

         <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Data Konfirmasi Docker Image<sup class="text-danger">*</sup></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover" id="dockerTable" >
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Proyek</th>
                                <th>Nama Ketua</th>
                                <th>Nama Pembimbing</th>
                                <th>E-mail</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>

                    </div>
                    </div>
         </div>

         <div class="col-lg-12">
            <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Data Docker Image Terkonfirmasi<sup class="text-danger">*</sup></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
            <table class="table table-striped table-bordered table-hover" id="cDockerTable" >
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Proyek</th>
                        <th>Nama Ketua</th>
                        <th>Nama Pembimbing</th>
                        <th>E-mail</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>

            </div>
            </div>
        </div>
      </div>

    </div>

{{-- @endif --}}
@stop

@push('scripts')
@if(Session::has('anggota'))
<script>
   $(document).ready(function() {
       toastr.options = {
           closeButton: true,
           progressBar: true,
           preventDuplicates: true,
           positionClass: 'toast-top-right',
       };
       toastr.success('{{ Session::get('anggota') }}', 'Berhasil!');

   });
</script>
@endif
<!-- Jquery Validate -->
<script src="{{ asset('master/js/plugins/validate/jquery.validate.min.js') }}"></script>

<!-- SWAL -->
<script src="{{ asset('master/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script>
    $(document).ready(function(){

    // Komentar Docker
    $(document).on('click', '.komentarDocker', function(){
        var id = $(this).data('id');
        $('#idKomentarDocker').val(id);
    });

    // Detail Docker
    $(document).on('click', '.detailDocker', function(){
        var id = $(this).data('id');
        $.ajax({
            url: "{{ route('detail.docker.admin') }}",
            method: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
            },
            data: { id: id },
            success: function(data){
                $('#detailTable tbody').remove();
                $.each(data, function(i, item){
                    i++;
                    $('#detailTable').append('<tr><td>'+ i +'<td>'+ item.konteks + '<td>' + '<a href="'+ item.dockerImage +'">' + item.dockerImage)
                })
            },
            error: function(){
                swal("ERROR", "Terjadi kesalahan pada saat mengkonfirmasi!", "error");
            }
        });
    });

    //Confirm Docker
    $(document).on('click', '.cDocker', function () {
         var ang_id = $(this).data('id');
          swal({
                      title: "Anda Yakin?",
                      text: "Docker Image yang didaftarkan tidak terkendala sama sekali?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Ya, Konfirmasi!",
                      cancelButtonText: "Tidak!",
                      closeOnConfirm: false,
                      closeOnCancel: false },
                  function (isConfirm) {
                      if (isConfirm) {
                           $.ajax({
                              url: "{{ route('confirm.docker.admin') }}",
                              method: "POST",
                              headers: {
                                 "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                              },
                              data: { id: ang_id },
                              success: function(){
                                 swal("Dikonfirmasi!", "Berhasi dikonfirmasi!.", "success");
                                 setTimeout(function(){
                                    window.location.reload();
                                 }, 1500);
                              },
                              error: function(){
                                 swal("ERROR", "Terjadi kesalahan pada saat mengkonfirmasi!", "error");
                              }
                           });
                      } else {
                          swal("Dibatalkan", "Batal di konfirmasi", "error");
                      }
                  });
            });
        // END CONFIRM

    //Batal Confirm Docker
    $(document).on('click', '.ucDocker', function () {
         var ang_id = $(this).data('id');
          swal({
                      title: "Anda Yakin?",
                      text: "Batalkan konfirmasi? Ada kesalahan?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Ya, Batal Konfirmasi!",
                      cancelButtonText: "Tidak!",
                      closeOnConfirm: false,
                      closeOnCancel: false },
                  function (isConfirm) {
                      if (isConfirm) {
                           $.ajax({
                              url: "{{ route('unconfirm.docker.admin') }}",
                              method: "POST",
                              headers: {
                                 "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                              },
                              data: { id: ang_id },
                              success: function(){
                                 swal("Dibatalkan!", "Konfirmasi dibatalkan!.", "success");
                                 setTimeout(function(){
                                    window.location.reload();
                                 }, 1500);
                              },
                              error: function(){
                                 swal("ERROR", "Terjadi kesalahan pada saat membatalkan permintaan!", "error");
                              }
                           });
                      } else {
                          swal("Dibatalkan", "Dibatalkan", "error");
                      }
                  });
            });
        // END CONFIRM

    //Edit Anggota
    $(document).on('click', '.editRepo', function () {
         var ang_id = $(this).data('id');
               $.ajax({
                  url: "{{ route('dataEdit.repo.user') }}",
                  method: "POST",
                  headers: {
                     "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                  },
                  data: { id: ang_id },
                  success: function(data){
                     $('#aidiR').val(ang_id);
                     $('#edKonteks').val(data[0].konteks);
                     $('#edTech').val(data[0].tech);
                  },
                  error: function(){
                     swal("ERROR", "Terjadi kesalahan pada saat mengupdate data!", "error");
                  }
               });
      });
      //END EDIT

    //Delete Repo
    $(document).on('click', '.deleteRepo', function () {
         var ang_id = $(this).data('id');
          swal({
                      title: "Anda Yakin?",
                      text: "Data yang sudah dihapus tidak dapat dikembalikan lagi!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Ya, Hapus!",
                      cancelButtonText: "Tidak!",
                      closeOnConfirm: false,
                      closeOnCancel: false },
                  function (isConfirm) {
                      if (isConfirm) {
                           $.ajax({
                              url: "{{ route('del.repo.user') }}",
                              method: "POST",
                              headers: {
                                 "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                              },
                              data: { id: ang_id },
                              success: function(){
                                 swal("Dihapus!", "Data yang anda pilih berhasil dihapus!.", "success");
                                 setTimeout(function(){
                                    window.location.reload();
                                 }, 1500);
                              },
                              error: function(){
                                 swal("ERROR", "Terjadi kesalahan pada saat menghapus data!", "error");
                              }
                           });
                      } else {
                          swal("Dibatalkan", "Data yang anda pilih Tidak Jadi Dihapus", "error");
                      }
                  });
            });
        // END DELETE

    //Edit Anggota
    $(document).on('click', '.editAnggota', function () {
         var ang_id = $(this).data('id');
               $.ajax({
                  url: "{{ route('dataEdit.anggota.user') }}",
                  method: "POST",
                  headers: {
                     "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                  },
                  data: { id: ang_id },
                  success: function(data){
                     $('#aidi').val(ang_id);
                     $('#edNama_lengkap').val(data[0].nama_lengkap);
                     $('#edUsername').val(data[0].username);
                     $('#edEmail').val(data[0].email);
                  },
                  error: function(){
                     swal("ERROR", "Terjadi kesalahan pada saat menghapus data!", "error");
                  }
               });
      });
      //END EDIT

    //Delete Anggota
      $(document).on('click', '.deleteAnggota', function () {
         var ang_id = $(this).data('id');
          swal({
                      title: "Anda Yakin?",
                      text: "Data yang sudah dihapus tidak dapat dikembalikan lagi!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Ya, Hapus!",
                      cancelButtonText: "Tidak!",
                      closeOnConfirm: false,
                      closeOnCancel: false },
                  function (isConfirm) {
                      if (isConfirm) {
                           $.ajax({
                              url: "{{ route('del.anggota.user') }}",
                              method: "POST",
                              headers: {
                                 "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                              },
                              data: { id: ang_id },
                              success: function(){
                                 swal("Dihapus!", "Data yang anda pilih berhasil dihapus!.", "success");
                                 setTimeout(function(){
                                    window.location.reload();
                                 }, 1500);
                              },
                              error: function(){
                                 swal("ERROR", "Terjadi kesalahan pada saat menghapus data!", "error");
                              }
                           });
                      } else {
                          swal("Dibatalkan", "Data yang anda pilih Tidak Jadi Dihapus", "error");
                      }
                  });
            });
        // END DELETE

        $('#dockerTable')
            .on( 'draw.dt', function () {
                console.log( 'Loading' );
            } )
            .on( 'init.dt', function () {
                console.log( 'Loaded' );
                $("#MessageContainer").html("")
            } )
            .DataTable({
                columnDefs: [{
                   defaultContent: "-",
                   targets: "_all"
                }],
                responsive: true,
                prossessing: true,
                serverSide: true,
                ajax: '{!! route('data.docker.admin') !!}',
                columns: [
                  { name: '', data: 'DT_RowIndex' },
                  { name: 'nama_proyek', data: 'nama_proyek' },
                  { name: 'nama_ketua', data: 'nama_ketua' },
                  { name: 'nama_pembimbing', data: 'nama_pembimbing' },
                  { name: 'email', data: 'email' },
                  {
                     name: 'action',
                     data: 'action',
                     sortable: false
                  },
               ]
            });

        $('#cDockerTable')
            .on( 'draw.dt', function () {
                console.log( 'Loading' );
            } )
            .on( 'init.dt', function () {
                console.log( 'Loaded' );
                $("#MessageContainer").html("")
            } )
            .DataTable({
                columnDefs: [{
                   defaultContent: "-",
                   targets: "_all"
                }],
                responsive: true,
                prossessing: true,
                serverSide: true,
                ajax: '{!! route('dataConfirm.docker.admin') !!}',
                columns: [
                  { name: '', data: 'DT_RowIndex' },
                  { name: 'nama_proyek', data: 'nama_proyek' },
                  { name: 'nama_ketua', data: 'nama_ketua' },
                  { name: 'nama_pembimbing', data: 'nama_pembimbing' },
                  { name: 'email', data: 'email' },
                  {
                     name: 'action',
                     data: 'action',
                     sortable: false
                  },
               ]
            });
        });
</script>
@endpush
