@extends('layouts.dashboard')
@extends('admin.menu')

@section('title', 'KEJURLAT 2019 | Dashboard User')
@push('styles')

@endpush
@section('menus')
    <li>
        <a href="{{ route('dashboard.admin') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('verify.admin') }}"><i class="fa fa-tasks"></i> <span class="nav-label">Kelompok Proyek</span></a>
    </li>
    <li>
        <a href="{{ route('gitlab.admin') }}"><i class="fab fa-gitlab"></i> <span class="nav-label">Gitlab & Chat Room</span></a>
    </li>
    <li>
        <a href="{{ route('docker.admin') }}"><i class="fab fa-docker"></i> <span class="nav-label">Konfigurasi Docker</span></a>
    </li>
    <li class="active">
        <a href="{{ route('info.admin') }}"><i class="fa fa-bullhorn"></i> <span class="nav-label">Pemberitahuan</span></a>
    </li>
@stop
@section('content')
<!-- breadcrumb -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Informasi | Proyek <b class="text-navy">{{ Auth::user()->nama_proyek }}</b></h2>
        <ol class="breadcrumb">
           <li>
               <a href="{{ route('dashboard.user') }}">Dashboard</a>
           </li>
            <li class="active">
                <strong>Informasi</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-sm-8">
            <div class="ibox">
                <div class="ibox-content">
                    <span class="text-muted small pull-right">Tanggal: <i class="fa fa-clock-o"></i> {{ date('d-m-Y h:i:s') }}</span>
                    <h2><i class="fas fa-info-circle"></i> Informasi</h2>
                    <p>
                        All clients need to be verified before you can send email and set a project.
                    </p>
                    <div class="clients-list">
                    <ul class="nav nav-tabs">
                        <span class="pull-right small text-muted">Total: Akun</span>
                        <li class="active"><a data-toggle="tab" href="#tab-1"><i class="fab fa-gitlab"></i> Gitlab</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2"><i class="fab fa-git-alt"></i> Repository</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"><i class="fab fa-docker"></i> Docker Image</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="full-height-scroll">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="full-height-scroll">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="full-height-scroll">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="ibox ">

                <div class="ibox-content">
                    <div class="tab-content">
                        <div id="contact-1" class="tab-pane active">
                            <div class="client-detail">
                            <div class="full-height-scroll">

                                <h3>Informasi Akun/Proyek</h3>
                                <div class="col-sm-12">
                                    <ul class="list-group clear-list">
                                        <li class="list-group-item fist-item">
                                            <b class="pull-right">  </b>
                                            Nama Proyek
                                        </li>
                                        <li class="list-group-item">
                                            <b class="pull-right">  </b>
                                            Nama Ketua
                                        </li>
                                        <li class="list-group-item">
                                            <b class="pull-right">  </b>
                                            Nama Pembimbing
                                        </li>
                                        <li class="list-group-item">
                                            <b class="pull-right">  </b>
                                            Email Ketua
                                        </li>
                                        <li class="list-group-item">
                                            <b class="pull-right">  </b>
                                            No WhatsApp Ketua
                                        </li>
                                    </ul>
                                <strong>Notes</strong>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <hr/>
                                </div>
                                <strong>Timeline activity</strong>
                                <div id="vertical-timeline" class="vertical-container dark-timeline">
                                    @component('component.timeline')
                                        @slot('icon', 'fab fa-gitlab')
                                           @if(Auth::user()->progress == 0)
                                              @slot('bg', 'lazur-bg')
                                              @slot('btn', 'info')
                                              @slot('btn_text', 'Konfigurasi Sekarang')
                                              @slot('displai', '')
                                              @slot('tgl_1', 'Segera Lengkapi Data!')
                                           @elseif(Auth::user()->progress == 15)
                                              @slot('bg', 'yellow-bg')
                                              @slot('btn', 'grey')
                                              @slot('btn_text', '')
                                              @slot('displai', 'display: none;')
                                              @slot('tgl_1', 'Menunggu Konfirmasi...')
                                           @elseif(Auth::user()->progress == 30 || Auth::user()->progress > 30 )
                                              @slot('bg', 'blue-bg')
                                              @slot('btn', 'grey')
                                              @slot('btn_text', '')
                                              @slot('displai', 'display: none;')
                                              @slot('tgl_1', 'Konfigurasi Selesai')
                                           @endif
                                        @slot('title', 'Konfigurasi Gitlab & Chat Room')
                                        @slot('text', '')
                                        @slot('tgl_2', date('d-m-Y'))
                                        @slot('route', route('gitlab.user'))
                                    @endcomponent
                                    @component('component.timeline')
                                        @slot('icon', 'fab fa-docker')
                                           @if(Auth::user()->progress >= 15)
                                              @slot('bg', 'lazur-bg')
                                              @slot('btn', 'info')
                                              @slot('btn_text', 'Konfigurasi Sekarang')
                                              @slot('displai', '')
                                              @slot('tgl_1', 'Segera Lengkapi Data!')
                                           @elseif(Auth::user()->progress >= 60)
                                              @slot('bg', 'blue-bg')
                                              @slot('btn', 'grey')
                                              @slot('btn_text', '')
                                              @slot('displai', 'display: none;')
                                              @slot('tgl_1', 'Selesai Melengkapi Data')
                                           @else
                                              @slot('bg', 'red-bg')
                                              @slot('btn', 'primary')
                                              @slot('btn_text', '')
                                              @slot('tgl_1', 'Belum Melengkapi Data!')
                                              @slot('displai', 'display: none;')
                                           @endif
                                        @slot('title', 'Konfigurasi Docker')
                                        @slot('text', '')
                                        @slot('tgl_2', date('d-m-Y'))
                                        @slot('route', '')
                                    @endcomponent

                                    @component('component.timeline')
                                        @slot('icon', 'fab fa-jenkins')
                                           @if(Auth::user()->progress == 60)
                                              @slot('bg', 'lazur-bg')
                                              @slot('btn', 'info')
                                              @slot('btn_text', 'Konfigurasi Sekarang')
                                              @slot('displai', '')
                                              @slot('tgl_1', 'Segera Konfigurasi!')
                                           @elseif(Auth::user()->progress == 75)
                                              @slot('bg', 'yellow-bg')
                                              @slot('btn', 'grey')
                                              @slot('btn_text', '')
                                              @slot('displai', 'display: none;')
                                              @slot('tgl_1', 'Menunggu Konfirmasi...')
                                           @elseif(Auth::user()->progress == 100)
                                              @slot('bg', 'blue-bg')
                                              @slot('btn', 'grey')
                                              @slot('btn_text', '')
                                              @slot('displai', 'display: none;')
                                              @slot('tgl_1', 'Selesai Mengkonfigurasi Jenkins')
                                           @else
                                              @slot('bg', 'red-bg')
                                              @slot('btn', 'primary')
                                              @slot('btn_text', '')
                                              @slot('tgl_1', 'Belum Mengupload Bukti Pembayaran!')
                                              @slot('displai', 'display: none;')
                                           @endif
                                        @slot('title', 'Pembayaran')
                                        @slot('text', '')
                                        @slot('tgl_2', date('d-m-Y'))
                                        @slot('route', '')
                                    @endcomponent

                                    @component('component.timeline')
                                        @slot('icon', 'fa fa-clock')
                                           @if(Auth::user()->progress == 100)
                                              @slot('bg', 'yellow-bg')
                                              @slot('btn', 'primary')
                                              @slot('btn_text', '')
                                              @slot('displai', 'display: none;')
                                              @slot('tgl_1', 'Pengumuman')
                                           @else
                                              @slot('bg', 'red-bg')
                                              @slot('btn', 'primary')
                                              @slot('btn_text', '')
                                              @slot('tgl_1', 'Menunggu Pengumuman...')
                                              @slot('displai', 'display: none;')
                                           @endif
                                        @slot('title', 'Menunggu Pengumuman Selanjutnya...')
                                        @slot('text', '')
                                        @slot('tgl_2', date('d-m-Y'))
                                        @slot('route', '')
                                    @endcomponent
                                 </div>
                            </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Info Panel -->
          <div class="info2">
            <div class="col-sm-4 pull-left">
                <div class="panel panel-info">
                    <div class="panel-heading">
                         <i class="fa fa-info-circle"></i> Info
                    </div>
                    <div class="panel-body">
                       <div class="warning"><i class="fa fa-circle text-danger"></i> <span>Password Default Gitlab : <strong class="text-danger"></strong></span></div>
                       <div class="warning"><i class="fa fa-circle text-info"></i> <span>Link Group Repository: <a class="text-navy" href="" target="_blank"><b></b></a></span></div>
                       <div class="warning"><i class="fa fa-circle text-info"></i> <span>Link Login Chat Room: <a class="text-navy" href="" target="_blank"><b></b></a></span></div>
                    </div>
                </div>
            </div>
          </div>
      </div>
</div>

@stop
@push('scripts')
<!-- Full Calendar -->
<script src="{{ asset('master/js/plugins/fullcalendar/moment.min.js') }}"></script>
<script src="{{ asset('master/js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>

<script>
    $(document).ready(function() {

    });

</script>
@endpush
