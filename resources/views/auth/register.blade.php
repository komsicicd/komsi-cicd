@extends('layouts.app')

@section('content')
<div class="middle-box text-center loginscreen   animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name"><img width="200px" src="https://3.bp.blogspot.com/--hpDlKXCqIY/XAqwsbdsXOI/AAAAAAAAAM4/ku-yr8gRk6E4jAtMRv98w7pgYVVOeCJMQCLcBGAs/s1600/komsi.jpg" alt="logo himakomsi"></h1>

        </div>
        <h3>Daftarkan Proyekmu</h3>
        <p>Manajemen Kelompok Proyek dan Konfigurasi CI/CD untuk mahasiswa KOMSI & TPRL SV UGM</p>
        <form class="m-t" role="form" method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-group">
                <input placeholder="Nama Proyek" id="nama_proyek" type="text" class="form-control @error('nama_proyek') is-invalid @enderror" name="nama_proyek" value="{{ old('nama_proyek') }}" required autocomplete="nama_proyek" autofocus>

                @error('nama_proyek')
                    <span class="invalid-feedback text-danger text-left" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input placeholder="Ketua Kelompok" id="nama_ketua" type="text" class="form-control @error('nama_ketua') is-invalid @enderror" name="nama_ketua" value="{{ old('nama_ketua') }}" required autocomplete="nama_ketua" autofocus>

                @error('nama_ketua')
                    <span class="invalid-feedback text-danger text-left" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input placeholder="Pembimbing Proyek" id="nama_pembimbing" type="text" class="form-control @error('nama_ketua') is-invalid @enderror" name="nama_pembimbing" value="{{ old('nama_pembimbing') }}" required autocomplete="nama_pembimbing" autofocus>

                @error('nama_pembimbing')
                    <span class="invalid-feedback text-danger text-left" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback text-danger text-left" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group input-group">
                <span class="input-group-addon">+62</span>
                <input placeholder="No WhatsApp" id="no_telp" type="number" class="form-control @error('no_telp') has-error @enderror" name="no_telp" value="{{ old('no_telp') }}" required autocomplete="no_telp">

                @error('no_telp')
                    <span class="invalid-feedback text-danger text-left" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback text-danger text-left" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input placeholder="Komfirmasi Password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Daftar</button>

            <p class="text-muted text-center"><small>Sudah Punya Akun?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="{{ route('login') }}">Masuk</a>
        </form>
        <p class="m-t"> <small>Tugas Akhir KOMSI CI/CD &copy; 2020</small> </p>
    </div>
</div>
@endsection
