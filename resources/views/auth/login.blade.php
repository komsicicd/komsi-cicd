@extends('layouts.app')

@section('content')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name"><img width="200px" src="https://3.bp.blogspot.com/--hpDlKXCqIY/XAqwsbdsXOI/AAAAAAAAAM4/ku-yr8gRk6E4jAtMRv98w7pgYVVOeCJMQCLcBGAs/s1600/komsi.jpg" alt="logo himakomsi"></h1>

        </div>
        <h3>KOMSI CI/CD</h3>
        <p>Manajemen Kelompok Proyek KOMSI & TPRL SV UGM dan Konfigurasi CI/CD
        </p>
        {{-- <p>Login in. To see it in action.</p> --}}
        <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">
                Masuk
            </button>

            <div class="row">
                <div class="col-md-5">
                    <div class="form-check">
                        <input class="i-checks" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label style="font-size:11px" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
                <div class="col-md-7">
                    @if (Route::has('password.request'))
                        <a style="font-size:12px" class="text-navy" href="{{ route('password.request') }}">
                            Lupa Password?
                        </a>
                    @endif
                </div>
            </div>
            <br>
            <p class="text-muted text-center"><small>Belum punya akun?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="{{ route('register') }}">Daftar Sekarang</a>
        </form>
        <p class="m-t"> <small>Tugas Akhir KOMSI CI/CD &copy; 2020</small> </p>
    </div>
</div>
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
