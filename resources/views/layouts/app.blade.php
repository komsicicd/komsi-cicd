<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Login</title>

    <link href="{{ asset('master/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('master/font-awesome/css/all.css') }}" rel="stylesheet">

    <link href="{{ asset('master/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('master/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('master/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('master/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">

</head>

<body class="gray-bg">
    @yield('content')
    <!-- Mainly scripts -->
    <script src="{{ asset('master/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('master/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('master/js/plugins/iCheck/icheck.min.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>

</body>
</html>
