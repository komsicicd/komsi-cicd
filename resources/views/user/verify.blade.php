@extends('layouts.app')

@section('content')
    <div class="middle-box text-center  animated fadeInDown">
        <div>
            <div class="m-b-md">
            <img width="150px" src="https://3.bp.blogspot.com/--hpDlKXCqIY/XAqwsbdsXOI/AAAAAAAAAM4/ku-yr8gRk6E4jAtMRv98w7pgYVVOeCJMQCLcBGAs/s1600/komsi.jpg" alt="logo himakomsi">
            </div>
            <h3>Menunggu Konfirmasi dari Admin</h3>
            <p>Proyek yang kamu daftarkan sedang dikonfirmasi dan diperiksa oleh admin, setelah terkonfirmasi kami akan memberi tahu anda.</p>
            <form class="m-t" method="POST" role="form" action="{{ route('logout') }}">
                @csrf
                <button type="submit" class="btn btn-primary"><i class="fas fa-sign-out-alt"></i> Keluar</button>
            </form>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
@endsection
