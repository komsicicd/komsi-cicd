@extends('layouts.dashboard')
@extends('admin.menu')

@section('title', 'KOMSI CI/CD | Dashboard User')
@push('styles')
<style media="screen">
   .info2{
      display: none;
   }
   @media only screen and (max-width: 1170px) {
     .info1{
        display: none;
     }
     .info2{
        display: block;
     }
   }
</style>
@endpush
@section('menus')
   <li class="active">
       <a href="{{ route('dashboard.user') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
   </li>
   <li>
       <a href="{{ route('gitlab.user') }}"><i class="fab fa-gitlab"></i> <span class="nav-label">Gitlab & Chat Room</span></a>
   </li>
   <li>
       <a href="{{ route('docker.user') }}"><i class="fab fa-docker"></i> <span class="nav-label">Konfigurasi Docker</span> <span class="pull-right label label-primary" style="display: {{ Auth::user()->progress >= 30 ? 'none' : '' }}">!</span></a>
   </li>
   <li>
       <a href="{{ route('info.user') }}"><i class="fas fa-info-circle"></i> <span class="nav-label">Informasi</span></a>
   </li>
@stop
@section('content')
    <!-- timeline -->
    <div class="wrapper wrapper-content animated fadeInRight">
       <div class="row" style="margin-bottom: 20px;">
          <div class="col-lg-12">
                   <div class="ibox-content" id="ibox-content">
                      <div class="row" style="padding-top: 20px;">
                         <div class="col-lg-12">
                             <dl class="dl-horizontal">
                                 <dt>Progress Konfigurasi:</dt>
                                 <dd>
                                     <div class="progress progress-striped active m-b-sm">
                                         <div style="width: {{ Auth::user()->progress }}%;" class="progress-bar"></div>
                                     </div>
                                     @if(Auth::user()->progress == 100)
                                       <small>Proses Konfigurasi <strong>{{ Auth::user()->progress }}%</strong>. Menunggu Pengumuman Selanjutnya.. </small>
                                     @else
                                       <small>Proses Konfigurasi <strong>{{ Auth::user()->progress }}%</strong> Selesai. Segera lengkapi data dan konfigurasi sesuai petunjuk.</small>
                                     @endif
                                 </dd>
                             </dl>
                         </div>
                     </div>
                      <!-- Info Panel -->
                      <div class="info1">
                        <div class="col-sm-3 pull-right">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                     <i class="fa fa-info-circle"></i> Info
                                </div>
                                <div class="panel-body">
                                   <div class="danger"><i class="fa fa-circle text-navy"></i> <span>Belum Selesai</span></div>
                                   <div class="danger"><i class="fa fa-circle text-info"></i> <span>Periode Saat Ini</span></div>
                                   <div class="warning"><i class="fa fa-circle text-warning"></i> <span>Menunggu Konfirmasi</span></div>
                                   <div class="success"><i class="fa fa-circle text-success"></i> <span>Selesai</span></div>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="info2">
                        <div class="row">
                        <div class="col-sm-6 pull-left">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                     <i class="fa fa-info-circle"></i> Info
                                </div>
                                <div class="panel-body">
                                   <div class="danger"><i class="fa fa-circle text-navy"></i> <span>Belum Selesai</span></div>
                                   <div class="danger"><i class="fa fa-circle text-info"></i> <span>Periode Saat Ini</span></div>
                                   <div class="warning"><i class="fa fa-circle text-warning"></i> <span>Menunggu Konfirmasi</span></div>
                                   <div class="success"><i class="fa fa-circle text-success"></i> <span>Selesai</span></div>
                                </div>
                            </div>
                        </div>
                        </div>
                      </div>
                      <!-- end Info Panel -->

                       <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
                          @component('component.timeline')
                              @slot('icon', 'fab fa-gitlab')
                                 @if(Auth::user()->progress == 0)
                                    @slot('bg', 'lazur-bg')
                                    @slot('btn', 'info')
                                    @slot('btn_text', 'Konfigurasi Sekarang')
                                    @slot('displai', '')
                                    @slot('tgl_1', 'Segera Lengkapi Data!')
                                 @elseif(Auth::user()->progress == 15)
                                    @slot('bg', 'yellow-bg')
                                    @slot('btn', 'grey')
                                    @slot('btn_text', '')
                                    @slot('displai', 'display: none;')
                                    @slot('tgl_1', 'Menunggu Konfirmasi...')
                                 @elseif(Auth::user()->progress == 30 || Auth::user()->progress > 30 )
                                    @slot('bg', 'blue-bg')
                                    @slot('btn', 'grey')
                                    @slot('btn_text', '')
                                    @slot('displai', 'display: none;')
                                    @slot('tgl_1', 'Konfigurasi Selesai')
                                 @endif
                              @slot('title', 'Konfigurasi Gitlab & Chat Room')
                              @slot('text', 'Mengisi data anggota kelompok proyek untuk di daftarkan akun Gitlab dan Mattermost Chat Room.')
                              @slot('tgl_2', date('d-m-Y'))
                              @slot('route', route('gitlab.user'))
                          @endcomponent
                          @component('component.timeline')
                              @slot('icon', 'fab fa-docker')
                                 @if(Auth::user()->progress == 30 || Auth::user()->progress == 45)
                                    @slot('bg', 'lazur-bg')
                                    @slot('btn', 'info')
                                    @slot('btn_text', 'Konfigurasi Sekarang')
                                    @slot('displai', '')
                                    @slot('tgl_1', 'Segera Lengkapi Data!')
                                 @elseif(Auth::user()->progress == 60)
                                    @slot('bg', 'yellow-bg')
                                    @slot('btn', 'grey')
                                    @slot('btn_text', '')
                                    @slot('displai', 'display: none;')
                                    @slot('tgl_1', 'Menunggu Konfirmasi...')
                                 @elseif(Auth::user()->progress == 80)
                                    @slot('bg', 'blue-bg')
                                    @slot('btn', 'grey')
                                    @slot('btn_text', '')
                                    @slot('displai', 'display: none;')
                                    @slot('tgl_1', 'Selesai Melengkapi Data')
                                 @else
                                    @slot('bg', 'red-bg')
                                    @slot('btn', 'primary')
                                    @slot('btn_text', '')
                                    @slot('tgl_1', 'Belum Melengkapi Data!')
                                    @slot('displai', 'display: none;')
                                 @endif
                              @slot('title', 'Konfigurasi Docker')
                              @slot('text', 'Melengkapi dan mengimplementasi prosedur-prosedur Docker seperti konfigurasi Dockerfile dan Push Docker to Registry untuk proses Deployment proyek.')
                              @slot('tgl_2', date('d-m-Y'))
                              @slot('route', route('docker.user'))
                          @endcomponent

                          @component('component.timeline')
                              @slot('icon', 'fab fa-jenkins')
                                 @if(Auth::user()->progress == 60)
                                    @slot('bg', 'lazur-bg')
                                    @slot('btn', 'info')
                                    @slot('btn_text', 'Konfigurasi Sekarang')
                                    @slot('displai', '')
                                    @slot('tgl_1', 'Segera Konfigurasi!')
                                 @elseif(Auth::user()->progress == 75)
                                    @slot('bg', 'yellow-bg')
                                    @slot('btn', 'grey')
                                    @slot('btn_text', '')
                                    @slot('displai', 'display: none;')
                                    @slot('tgl_1', 'Menunggu Konfirmasi...')
                                 @elseif(Auth::user()->progress == 100)
                                    @slot('bg', 'blue-bg')
                                    @slot('btn', 'grey')
                                    @slot('btn_text', '')
                                    @slot('displai', 'display: none;')
                                    @slot('tgl_1', 'Selesai Mengkonfigurasi Jenkins')
                                 @else
                                    @slot('bg', 'red-bg')
                                    @slot('btn', 'primary')
                                    @slot('btn_text', '')
                                    @slot('tgl_1', 'Belum Mengupload Bukti Pembayaran!')
                                    @slot('displai', 'display: none;')
                                 @endif
                              @slot('title', 'Pembayaran')
                              @slot('text', 'Pembayaran dilakukan dengan transfer sesuai jumlah yang tertera pada payment bill sesuai jumlah atlit yang didaftarkan dan sudah termasuk biaya kontingen dan administrasi.')
                              @slot('tgl_2', date('d-m-Y'))
                              @slot('route', '')
                          @endcomponent

                          @component('component.timeline')
                              @slot('icon', 'fa fa-clock')
                                 @if(Auth::user()->progress == 100)
                                    @slot('bg', 'yellow-bg')
                                    @slot('btn', 'primary')
                                    @slot('btn_text', '')
                                    @slot('displai', 'display: none;')
                                    @slot('tgl_1', 'Pengumuman')
                                 @else
                                    @slot('bg', 'red-bg')
                                    @slot('btn', 'primary')
                                    @slot('btn_text', '')
                                    @slot('tgl_1', 'Menunggu Pengumuman...')
                                    @slot('displai', 'display: none;')
                                 @endif
                              @slot('title', 'Menunggu Pengumuman Selanjutnya...')
                              @slot('text', '')
                              @slot('tgl_2', date('d-m-Y'))
                              @slot('route', '')
                          @endcomponent

                       </div>

                   </div>
          </div>
       </div>
    </div>
@stop
