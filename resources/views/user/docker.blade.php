@extends('layouts.dashboard')
@extends('user.menu')

@section('title', 'KEJURLAT 2019 | Dashboard User')
@push('styles')
<meta name="csrf" content="{{ csrf_token() }}">
<link href="{{asset('master/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">
<link href="{{asset('master/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{ asset('master/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
<!-- Ladda style -->
<link href="{{ asset('master/css/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

<style>

    .wizard > .content > .body  { position: relative; }

</style>
@endpush
@section('menus')
    <li>
        <a href="{{ route('dashboard.user') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('gitlab.user') }}"><i class="fab fa-gitlab"></i> <span class="nav-label">Gitlab & Chat Room</span></a>
    </li>
    <li class="active">
        <a href="{{ route('docker.user') }}"><i class="fab fa-docker"></i> <span class="nav-label">Konfigurasi Docker</span> <span class="pull-right label label-primary" style="display: {{ Auth::user()->progress >= 30 ? 'none' : '' }}">!</span></a>
    </li>
    <li>
        <a href="{{ route('info.user') }}"><i class="fas fa-info-circle"></i> <span class="nav-label">Informasi</span></a>
    </li>
@stop
@section('content')
      <!-- breadcrumb -->
      <div class="row wrapper border-bottom white-bg page-heading">
         <div class="col-lg-8">
              <h2>Konfigurasi Docker | Proyek <b class="text-navy">{{ Auth::user()->nama_proyek }}</b></h2>
              <ol class="breadcrumb">
                 <li>
                     <a href="{{ route('dashboard.user') }}">Dashboard</a>
                 </li>
                 <li class="active">
                     <strong>Konfigurasi Docker</strong>
                 </li>
              </ol>
         </div>
      </div>
         <!-- end breadcrumb -->

         <!-- Modal -->
            <!-- Link Image -->
      <div class="modal inmodal" id="linkImage" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
               <div class="modal-content animated fadeIn">
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <h3>Link Docker Image</h3>
                   </div>
                   <div class="modal-body">
                     <form method="POST" action="{{ route('image.docker.user') }}" class="tambah_repo_form">
                         @csrf
                        <input type="hidden" name="id" id="idImage">
                        <div class="form-group"><label>Link</label> <input placeholder="https://hub.docker.com/komsicicd/..." class="form-control" name="dockerImage"></div>
                   </div>
                         <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                         </div>
                      </form>
              </div>
            </div>
        </div>
     </div>
         <!-- End Modal -->

      <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Konfigurasi Docker & Dockerfile</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    @if(Auth::user()->progress == 80)
                    <div class="ibox-content" style="border-left: 4px solid #0097e6">
                        <h3>{{ $scmsg }} <i class="fa fa-check text-success"></i></h3>
                    </div>
                    @elseif(Auth::user()->progress == 60)
                    <div class="ibox-content" style="border-left: 4px solid #f9ca24">
                        <h3>Menunggu Konfirmasi Admin... <i class="fa fa-clock text-warning"></i></h3>
                    </div>
                    @elseif(Auth::user()->progress == 30)
                    <div class="ibox-content" style="border-left: 4px solid #1ab394">
                        <h3>Proyek {{ Auth::user()->nama_proyek }} secara keseluruhan berbasis Andorid & non-APi ?</h3>
                        <form action="{{ route('android.docker.user') }}" method="POST">
                            @csrf
                            <div class="radio radio-success radio-inline">
                                <input type="radio" id="inlineRadio1" value="option1" name="android">
                                <label for="inlineRadio1"> Ya </label>
                            </div>
                            <div class="radio radio-danger radio-inline">
                                <input type="radio" id="inlineRadio2" value="option2" name="android" checked>
                                <label for="inlineRadio2"> Tidak </label>
                            </div><br><br>
                            <button class="btn btn-primary" type="submit">Lanjutkan <i class="fa fa-angle-double-right"></i></button>
                        </form>
                    </div>
                    @elseif(Auth::user()->progress == 45)
                    <div class="ibox-content">
                        <h2>
                            Konfigurasi Docker & Dockerfile
                        </h2>
                        <h3 class="text-danger">
                            <sup>*</sup>Khusus Aplikasi Berbasis WEB & API
                        </h3>

                        <form id="formWizard" action="{{ route('done.docker.user') }}" method="POST" class="wizard-big">
                            <h1>Docker</h1>
                            <fieldset>
                                <h2>Install Docker pada PC/Laptop anda</h2>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Docker Install</label>
                                        <div class="well well-sm">
                                            <h4 id="copytext">Insall Docker di Windows & MacOS <a href="https://docs.docker.com/install/"><i class="fa fa-external-link-square-alt"></i></a></h4>
                                        </div>
                                        <label>Cek Versi Docker di Terminal/Command Prompt</label>
                                        <div class="well well-sm">
                                            <h4 style="font-family:'Courier New', Courier, monospace">$ Docker -version</h4>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <h1>Dockerfile</h1>
                            <fieldset>
                                <h2>Buat Dockerfile di Root Project Directory</h2>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Buat Dockerfile via Terminal/CMD</label>
                                        <div class="well well-sm">
                                            <h4 style="font-family:'Courier New', Courier, monospace">$ nano Dockerfile</h4>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-left: 3px" class="row">
                                    <label>Copy Template Dockerfile dibawah ini</label>
                                    <div class="col-md-12 faq-item">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <a data-toggle="collapse" href="#faq1" class="faq-question">Laravel App</a>
                                                <small>Aplikasi Laravel Fullstack atau WEB API</small>
                                            </div>
                                            <div class="col-md-3">
                                                <span class="small font-bold">Tech</span>
                                                <div class="tag-list">
                                                    <span class="tag-item">Laravel</span>
                                                    <span class="tag-item">API</span>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <span class="small font-bold">Versi </span><br/>
                                                5.8
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div id="faq1" class="panel-collapse collapse ">
                                                    <div class="faq-answer">
                                                        <pre style="font-family:'Courier New', Courier, monospace" id="laravelText">
FROM php:7.2.14-apache

#install all the system dependencies and enable PHP modules
RUN apt-get update && apt-get install -y \
libicu-dev \
libpq-dev \
libmcrypt-dev \
libpng-dev \
libfreetype6-dev \
libjpeg62-turbo-dev \
libgd-dev \
libxml2-dev \
git \
zip \
unzip \
gnupg \
libcurl4-openssl-dev \
&& rm -r /var/lib/apt/lists/* \
&& docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
&& docker-php-ext-configure mysqli --with-mysqli=mysqlnd \
&& docker-php-ext-install pdo_mysql \
&& docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
&& docker-php-ext-install gd \
&& docker-php-ext-install zip \
&& docker-php-ext-install tokenizer \
&& docker-php-ext-install mbstring \
&& docker-php-ext-install xml

RUN docker-php-ext-configure pgsql --with-pgsql=/usr/local/pgsql
RUN docker-php-ext-install pdo pgsql pdo_pgsql

RUN apt-get update && apt-get install -y libmemcached-dev \
&& pecl install memcached \
&& docker-php-ext-enable memcached.so
# RUN pecl channel-update pecl.php.net && pecl install memcached && docker-php-ext-enable memcached

#install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

#set our application folder as an environment variable
ENV APP_HOME /var/www/html

#change uid and gid of apache to docker user uid/gid
RUN usermod -u 1000 www-data && groupmod -g 1000 www-data

#change the web_root to laravel /var/www/html/public folder
RUN sed -i -e "s/html/html\/public/g" /etc/apache2/sites-enabled/000-default.conf

# enable apache module rewrite
RUN a2enmod rewrite

#copy source files and run composer
COPY --chown=1000:1000 . $APP_HOME

WORKDIR $APP_HOME

# install All Dependencies
RUN cp .env.staging .env
RUN export COMPOSER_ALLOW_SUPERUSER=1
RUN composer install --no-interaction
RUN php artisan key:generate
RUN php artisan cache:clear
# RUN php artisan migrate
# RUN php artisan db:seed
RUN php artisan config:cache
                                                        </pre>
                                                        <a href="#" class="btn btn-white copy" data-clipboard-target="laravelText"><i class="fa fa-copy"></i> Copy</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-left: 3px" class="row">
                                    <div class="col-md-12 faq-item">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <a data-toggle="collapse" href="#faq2" class="faq-question">ReactJS App</a>
                                                <small>Aplikasi React JS dengan module node & NPM</small>
                                            </div>
                                            <div class="col-md-3">
                                                <span class="small font-bold">Tech</span>
                                                <div class="tag-list">
                                                    <span class="tag-item">ReactJS</span>
                                                    <span class="tag-item">NPM</span>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <span class="small font-bold">Versi </span><br/>
                                                latest
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div id="faq2" class="panel-collapse collapse ">
                                                    <div class="faq-answer">
                                                        <pre style="font-family:'Courier New', Courier, monospace" id="reactText">
FROM hereabran/node-frontend as build-stage

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

FROM nginx:1.15

COPY --from=build-stage /usr/src/app/build/ /usr/share/nginx/html

COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf
                                                        </pre>
                                                        <a href="#" class="btn btn-white copy2" data-clipboard-target="reactText"><i class="fa fa-copy"></i> Copy</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-left: 3px" class="row">
                                    <div class="col-md-12 faq-item">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <a data-toggle="collapse" href="#faq3" class="faq-question">VueJS App</a>
                                                <small>Aplikasi Vue JS dengan module node & NPM</small>
                                            </div>
                                            <div class="col-md-3">
                                                <span class="small font-bold">Tech</span>
                                                <div class="tag-list">
                                                    <span class="tag-item">VueJS</span>
                                                    <span class="tag-item">NPM</span>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <span class="small font-bold">Versi </span><br/>
                                                latest
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div id="faq3" class="panel-collapse collapse ">
                                                    <div class="faq-answer">
                                                        <pre style="font-family:'Courier New', Courier, monospace" id="reactText">
FROM hereabran/node-frontend as build-stage

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

FROM nginx:1.15

COPY --from=build-stage /usr/src/app/build/ /usr/share/nginx/html

COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf
                                                        </pre>
                                                        <a href="#" class="btn btn-white copy2" data-clipboard-target="reactText"><i class="fa fa-copy"></i> Copy</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <h1>Build, Run and Ship</h1>
                            <fieldset>
                                <h2>Build, Run dan Push Docker Images ke Docker Registry</h2>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Build Dockerfile via Terminal/CMD</label>
                                        <div class="well well-sm">
                                            <h4 style="font-family:'Courier New', Courier, monospace">$ Docker build -t namaImage:tagImage .</h4>
                                        </div>
                                        <label>Run Docker via Terminal/CMD</label>
                                        <div class="well well-sm">
                                            <h4 style="font-family:'Courier New', Courier, monospace">$ Docker run -d --name namaContainer -p portHost:portContainer namaIname:tagImage</h4>
                                        </div>
                                        <label>Login ke Docker Registry</label>
                                        <div class="well well-sm">
                                            <h4 style="font-family:'Courier New', Courier, monospace">$ Docker login</h4>
                                        </div>
                                        <label>Push Docker Image ke Docker Registry </label><h4 class="text-danger"><i class="fas fa-info-circle"></i> Pastikan namespace Docker Image komsicicd/namaProyekMu</h4>
                                        <div class="well well-sm">
                                            <h4 style="font-family:'Courier New', Courier, monospace">$ Docker tag komsicicd/namaImage:tagImage</h4>
                                            <h4 style="font-family:'Courier New', Courier, monospace">$ Docker push komsicicd/namaImage:tagImage</h4>
                                        </div>
                                        <small class="text-primary"><sup>*</sup>Ganti namaContainer, portHost, portContainer, namaImage dan tagImage</small><br><br>
                                        <a class="btn btn-success" href="https://medium.com/@deepakshakya/beginners-guide-to-use-docker-build-run-push-and-pull-4a132c094d75" target="_black">Pelajari Lebih Lanjut <i class="fa fa-external-link-square-alt"></i></a>
                                    </div>
                                </div>
                            </fieldset>

                            <h1>Selesai</h1>
                            <fieldset>
                                <h2 class="text-danger"><b>PASTIKAN ANDA TELAH :</b></h2>
                                <h4><span class="label label-primary"><i class="fa fa-check"></i></span> Membuat Dockerfile sesuai prosedur</h4>
                                <h4><span class="label label-primary"><i class="fa fa-check"></i></span> Berhasil mem<i>build</i> Docker dan menjalankannya di localhost dengan lancar dan tidak terjadi error</h4>
                                <h4><span class="label label-primary"><i class="fa fa-check"></i></span> Memberi namespace Docker Image dengan konteks <b class="text-danger">komsicicd/namaProyekMu</b></h4>
                                <h4><span class="label label-primary"><i class="fa fa-check"></i></span> Push Docker Image tersebut ke Repository yang bisa dicek pada situs <a href="https://hub.docker.com/komsicicd" target="_blank">hub.docker.com/komsicicd</a></h4>
                                <h4><span class="label label-primary"><i class="fa fa-check"></i></span> Push Dockerfile ke Repository</h4>

                                <br>
                                <div class="row">
                                    <label>Link Docker Image <sup class="text-danger">*</sup></label><br>
                                    <div class="col-md-5 well well-sm">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Repository</th>
                                                    <th>Docker Image</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($repo as $rep)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $rep->konteks }}</td>
                                                    <td><a href="#" class="{{ $rep->dockerImage == '-' ? 'label label-warning imageLink' : '' }}" data-id="{{ $rep->id }}" data-toggle="{{ $rep->dockerImage == '-' ? 'modal' : '' }}" data-target="{{ $rep->dockerImage == '-' ? '#linkImage' : '' }}"><i class="fa fa-{{ $rep->dockerImage == '-' ? 'plus' : 'check' }}"></i>{{ $rep->dockerImage == '-' ? 'Link' : '' }}</a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="checkbox checkbox-success">
                                    <input id="yes" type="checkbox" name="yes" required>
                                    <label for="yes">
                                        Saya telah mengikuti semua langkah konfigurasi dan saya telah memastikan semua berjalan lancar.
                                    </label>
                                </div>
                                Ada kendala? Klik <a href="https://wa.me/087837440698">disini!</a>
                            </fieldset>
                        </form>
                    </div>
                    @elseif(Auth::user()->progress <= 30)
                    <div class="ibox-content" style="border-left: 4px solid #23c6c8">
                        <h3>Selesaikan Konfigurasi Gitlab & Chat Room. <a class="text-info" href="{{ route('gitlab.user') }}">Konfigurasi Sekarang <i class="fas fa-external-link-square-alt"></i> </a></h3>
                    </div>
                    @endif
                </div>
                </div>
            </div>
      </div>

@stop

@push('scripts')
@if(Session::has('anggota'))
<script>
   $(document).ready(function() {
       toastr.options = {
           closeButton: true,
           progressBar: true,
           preventDuplicates: true,
           positionClass: 'toast-top-right',
       };
       toastr.success('{{ Session::get('anggota') }}', 'Berhasil!');

   });
</script>
@endif
<!-- Jquery Validate -->
<script src="{{ asset('master/js/plugins/validate/jquery.validate.min.js') }}"></script>

<!-- SWAL -->
<script src="{{ asset('master/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Steps -->
<script src="{{ asset('master/js/plugins/staps/jquery.steps.min.js') }}"></script>

<!-- Clipboard -->
<script src="{{ asset('master/js/plugins/clipboard/clipboard.min.js') }}"></script>

<script>
    $(document).ready(function(){

        new Clipboard('.copy');
        new Clipboard('.copy2');

        $(document).on('click', '.imageLink', function(){
            var id = $(this).data('id');
            $('#idImage').val(id);
        });

        $("#formWizard").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    // var form = $(this);
                    @if(Auth::user()->progress < 30)
                        swal("Tidak Diizinkan!", "Konfigurasi Akun Gitlab proyekmu belum dikonfirmasi Admin!.", "error");
                    @else
                        var imageLink = $('#imageLink').val();
                        $.ajax({
                                url: "{{ route('done.docker.user') }}",
                                method: "POST",
                                headers: {
                                "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                                },
                                data: { prog: 15, imageLink: imageLink },
                                error: function(e){
                                console.error(e);
                                swal("Error", "Hubungi Administrator untuk Solusi Lebih lanjut.", "error");
                                }
                            });
                        swal("Berhasil!", "Data berhasil dikirim!.", "success");
                        setTimeout(function(){
                            window.location.href = "{{ route('dashboard.user') }}";
                        }, 1500);
                        @endif
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
        });
</script>
@endpush



