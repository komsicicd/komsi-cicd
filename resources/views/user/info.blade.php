@extends('layouts.dashboard')
@extends('user.menu')

@section('title', 'KEJURLAT 2019 | Dashboard User')
@push('styles')

@endpush
@section('menus')
    <li>
        <a href="{{ route('dashboard.user') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('gitlab.user') }}"><i class="fab fa-gitlab"></i> <span class="nav-label">Gitlab & Chat Room</span></a>
    </li>
    <li>
        <a href="{{ route('docker.user') }}"><i class="fab fa-docker"></i> <span class="nav-label">Konfigurasi Docker</span> <span class="pull-right label label-primary" style="display: {{ Auth::user()->progress >= 30 ? 'none' : '' }}">!</span></a>
    </li>
    <li class="active">
        <a href="{{ route('info.user') }}"><i class="fas fa-info-circle"></i> <span class="nav-label">Informasi</span></a>
    </li>
@stop
@section('content')
<!-- breadcrumb -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Informasi | Proyek <b class="text-navy">{{ Auth::user()->nama_proyek }}</b></h2>
        <ol class="breadcrumb">
           <li>
               <a href="{{ route('dashboard.user') }}">Dashboard</a>
           </li>
            <li class="active">
                <strong>Informasi</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-sm-8">
            <div class="ibox">
                <div class="ibox-content">
                    <span class="text-muted small pull-right">Tanggal: <i class="fa fa-clock-o"></i> {{ date('d-m-Y h:i:s') }}</span>
                    <h2><i class="fas fa-info-circle"></i> Informasi</h2>
                    <p>
                        All clients need to be verified before you can send email and set a project.
                    </p>
                    <div class="clients-list">
                    <ul class="nav nav-tabs">
                        <span class="pull-right small text-muted">Total: {{ count($anggota) }} Akun</span>
                        <li class="active"><a data-toggle="tab" href="#tab-4"><i class="fas fa-bullhorn"></i> Pemberitahuan</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-1"><i class="fab fa-gitlab"></i> Gitlab</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2"><i class="fab fa-git-alt"></i> Repository</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"><i class="fab fa-docker"></i> Docker Image</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-5"><i class="fas fa-comments"></i> Komentar/Revisi</a></li>
                    </ul>
                    <div class="tab-content active">
                        <div id="tab-4" class="tab-pane active">
                            <div class="full-height-scroll">
                                {{-- @foreach($notif as $not)

                                @endforeach --}}
                                <div class="project-list">

                                    <table class="table table-hover">
                                        <tbody>
                                        @foreach($notif as $not)
                                        <tr>
                                            <td class="project-status">
                                                <span class="label label-{{ $not->label }}">{{ $not->status }}</span>
                                            </td>
                                            <td class="project-title">
                                                <a href="{{ $not->cta }}" data-toggle="{{ $not->toggle }}">{{ $not->konten }}</a>
                                                <br>
                                                <small>Notified {{ $not->created_at }}</small>
                                            </td>
                                            <td class="project-completion">
                                                    <small>Progress: {{ $not->progress }}%</small>
                                                    <div class="progress progress-mini progress-striped active">
                                                        <div style="width: {{ $not->progress }}%;" class="progress-bar"></div>
                                                    </div>
                                            </td>
                                            <td class="project-people">

                                            </td>
                                            <td class="project-actions">
                                                <a style="display: {{ $not->is_config ? '' : 'none' }}" href="{{ $not->cta }}" class="btn btn-info btn-sm" data-toggle="{{ $not->toggle }}"><i class="{{ $not->cta_icon }}"></i> {{ $not->cta_text }} </a>
                                                <a href="#" class="btn btn-white btn-sm"><i class="fa fa-paper-plane"></i> Komplain </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td class="project-status" width="10%">
                                                <span class="label label-success">Verified</span>
                                            </td>
                                            <td class="project-title" width="40%">
                                                <a href="{{ route('gitlab.user') }}">Lengkapi Konfigurasi Gitlab & Chat Room Sekarang</a>
                                                <br>
                                                <small>Notified {{ date('d.m.Y') }}</small>
                                            </td>
                                            <td class="project-completion" width="20%">
                                                    <small>Progress: 0%</small>
                                                    <div class="progress progress-mini progress-striped active">
                                                        <div style="width: 0%;" class="progress-bar"></div>
                                                    </div>
                                            </td>
                                            <td class="project-people">

                                            </td>
                                            <td class="project-actions" width="30%">
                                                <a href="{{ route('gitlab.user') }}" class="btn btn-info btn-sm"><i class="fa fa-tools"></i> Konfigurasi </a>
                                                <a href="#" class="btn btn-white btn-sm"><i class="fa fa-paper-plane"></i> Komplain </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="tab-1" class="tab-pane">
                            <div class="full-height-scroll">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <th>#</th>
                                            <th>Nama Lengkap</th>
                                            <th>Username</th>
                                            <th></th>
                                            <th>Email</th>
                                            <th>Status</th>
                                        </thead>
                                        <tbody>
                                        @foreach($anggota as $angg)
                                            <tr>
                                                <td class="client-avatar">{{ $i++ }}</td>
                                                <td><b>{{ $angg->nama_lengkap }}</b></td>
                                                <td><i class="fa fa-user"></i> {{ $angg->username }}</td>
                                                <td class="contact-type"><i class="fa fa-envelope"> </i></td>
                                                <td> {{ $angg->email }}</td>
                                                <td class="client-status"><span class="label label-{{ $label }}">{{ $active }}</span></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="full-height-scroll">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <th>#</th>
                                            <th>Repository</th>
                                            <th>Teknologi</th>
                                            <th>Link</th>
                                            <th>Status</th>
                                        </thead>
                                        <tbody>
                                        @foreach($repo as $rep)
                                            <tr>
                                                <td>{{ $ia++ }}</td>
                                                <td><b>{{ $rep->konteks }}</b></td>
                                                <td>{{ $rep->tech }}</td>
                                                <td><a class="{{ $rep->link == '-' ? 'text-danger' : 'label label-success' }}" href="{{ $rep->link == '-' ? '#' : $rep->link }}" target="{{ $rep->link == '-' ? '' : '_blank' }}"><i class="fa fa-{{ $rep->link == '-' ? 'times' : 'link' }}"></i> {{ $rep->link == '-' ? '' : 'Link' }}</a></td>
                                                <td class="client-status"><span class="label label-{{ $label }}">{{ $active }}</span></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="full-height-scroll">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <th>#</th>
                                            <th>Repository</th>
                                            <th>Link Docker Image</th>
                                            <th>Status</th>
                                        </thead>
                                        <tbody>
                                        <span style="display: none">{{ $as = 1 }}</span>
                                        @foreach($repo as $rep)
                                            <tr>
                                                <td>{{ $as++ }}</td>
                                                <td><b>{{ $rep->konteks }}</b></td>
                                                <td><a class="{{ $rep->dockerImage == '-' || Auth::user()->progress < 80 ? 'text-danger' : 'label label-success' }}" href="{{ $rep->dockerImage == '-' || Auth::user()->progress < 80 ? '#' : $rep->dockerImage }}" target="{{ $rep->dockerImage == '-' || Auth::user()->progress < 80 ? '' : '_blank' }}"><i class="fa fa-{{ $rep->dockerImage == '-' || Auth::user()->progress < 80 ? 'times' : 'link' }}"></i> {{ $rep->dockerImage == '-' || Auth::user()->progress < 80 ? '' : 'Link Docker Image' }}</a></td>
                                                <td class="client-status"><span class="label label-{{ Auth::user()->progress == 80 ? 'primary' : 'warning' }}">{{ Auth::user()->progress == 80 ? 'Aktif' : 'Belum Aktif' }}</span></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="tab-5" class="tab-pane">
                            <div class="full-height-scroll">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <th>#</th>
                                            <th>Komentar/Revisi</th>
                                        </thead>
                                        <tbody>
                                        <span style="display: none">{{ $as = 1 }}</span>
                                        @foreach($komentar as $komen)
                                            <tr>
                                                <td>{{ $as++ }}</td>
                                                <td><b>{{ $komen->komentar }}</b></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="ibox ">

                <div class="ibox-content">
                    <div class="tab-content">
                        <div id="contact-1" class="tab-pane active">
                            <div class="client-detail">
                            <div class="full-height-scroll">

                                <h3>Informasi Akun/Proyek</h3>
                                <div class="col-sm-12">
                                    <ul class="list-group clear-list">
                                        <li class="list-group-item fist-item">
                                            <b class="pull-right"> {{ $ang->nama_proyek }} </b>
                                            Nama Proyek
                                        </li>
                                        <li class="list-group-item">
                                            <b class="pull-right"> {{ $ang->nama_ketua }} </b>
                                            Nama Ketua
                                        </li>
                                        <li class="list-group-item">
                                            <b class="pull-right"> {{ $ang->nama_pembimbing }} </b>
                                            Nama Pembimbing
                                        </li>
                                        <li class="list-group-item">
                                            <b class="pull-right"> {{ $ang->email }} </b>
                                            Email Ketua
                                        </li>
                                        <li class="list-group-item">
                                            <b class="pull-right"> +62{{ $ang->no_telp }} </b>
                                            No WhatsApp Ketua
                                        </li>
                                    </ul>
                                <strong>Notes</strong>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <hr/>
                                </div>
                                <strong>Timeline activity</strong>
                                <div id="vertical-timeline" class="vertical-container dark-timeline">
                                    @component('component.timeline')
                                        @slot('icon', 'fab fa-gitlab')
                                           @if(Auth::user()->progress == 0)
                                              @slot('bg', 'lazur-bg')
                                              @slot('btn', 'info')
                                              @slot('btn_text', 'Konfigurasi Sekarang')
                                              @slot('displai', '')
                                              @slot('tgl_1', 'Segera Lengkapi Data!')
                                           @elseif(Auth::user()->progress == 15)
                                              @slot('bg', 'yellow-bg')
                                              @slot('btn', 'grey')
                                              @slot('btn_text', '')
                                              @slot('displai', 'display: none;')
                                              @slot('tgl_1', 'Menunggu Konfirmasi...')
                                           @elseif(Auth::user()->progress == 30 || Auth::user()->progress > 30 )
                                              @slot('bg', 'blue-bg')
                                              @slot('btn', 'grey')
                                              @slot('btn_text', '')
                                              @slot('displai', 'display: none;')
                                              @slot('tgl_1', 'Konfigurasi Selesai')
                                           @endif
                                        @slot('title', 'Konfigurasi Gitlab & Chat Room')
                                        @slot('text', '')
                                        @slot('tgl_2', date('d-m-Y'))
                                        @slot('route', route('gitlab.user'))
                                    @endcomponent
                                    @component('component.timeline')
                                    @slot('icon', 'fab fa-docker')
                                        @if(Auth::user()->progress == 30 || Auth::user()->progress == 45)
                                            @slot('bg', 'lazur-bg')
                                            @slot('btn', 'info')
                                            @slot('btn_text', 'Konfigurasi Sekarang')
                                            @slot('displai', '')
                                            @slot('tgl_1', 'Segera Lengkapi Data!')
                                        @elseif(Auth::user()->progress == 60)
                                            @slot('bg', 'yellow-bg')
                                            @slot('btn', 'grey')
                                            @slot('btn_text', '')
                                            @slot('displai', 'display: none;')
                                            @slot('tgl_1', 'Menunggu Konfirmasi...')
                                        @elseif(Auth::user()->progress == 80)
                                            @slot('bg', 'blue-bg')
                                            @slot('btn', 'grey')
                                            @slot('btn_text', '')
                                            @slot('displai', 'display: none;')
                                            @slot('tgl_1', 'Selesai Melengkapi Data')
                                        @else
                                            @slot('bg', 'red-bg')
                                            @slot('btn', 'primary')
                                            @slot('btn_text', '')
                                            @slot('tgl_1', 'Belum Melengkapi Data!')
                                            @slot('displai', 'display: none;')
                                        @endif
                                    @slot('title', 'Konfigurasi Docker')
                                    @slot('text', 'Melengkapi dan mengimplementasi prosedur-prosedur Docker seperti konfigurasi Dockerfile dan Push Docker to Registry untuk proses Deployment proyek.')
                                    @slot('tgl_2', date('d-m-Y'))
                                    @slot('route', route('docker.user'))
                                @endcomponent

                                    @component('component.timeline')
                                        @slot('icon', 'fab fa-jenkins')
                                           @if(Auth::user()->progress == 60)
                                              @slot('bg', 'lazur-bg')
                                              @slot('btn', 'info')
                                              @slot('btn_text', 'Konfigurasi Sekarang')
                                              @slot('displai', '')
                                              @slot('tgl_1', 'Segera Konfigurasi!')
                                           @elseif(Auth::user()->progress == 75)
                                              @slot('bg', 'yellow-bg')
                                              @slot('btn', 'grey')
                                              @slot('btn_text', '')
                                              @slot('displai', 'display: none;')
                                              @slot('tgl_1', 'Menunggu Konfirmasi...')
                                           @elseif(Auth::user()->progress == 100)
                                              @slot('bg', 'blue-bg')
                                              @slot('btn', 'grey')
                                              @slot('btn_text', '')
                                              @slot('displai', 'display: none;')
                                              @slot('tgl_1', 'Selesai Mengkonfigurasi Jenkins')
                                           @else
                                              @slot('bg', 'red-bg')
                                              @slot('btn', 'primary')
                                              @slot('btn_text', '')
                                              @slot('tgl_1', 'Belum Mengupload Bukti Pembayaran!')
                                              @slot('displai', 'display: none;')
                                           @endif
                                        @slot('title', 'Pembayaran')
                                        @slot('text', '')
                                        @slot('tgl_2', date('d-m-Y'))
                                        @slot('route', '')
                                    @endcomponent

                                    @component('component.timeline')
                                        @slot('icon', 'fa fa-clock')
                                           @if(Auth::user()->progress == 100)
                                              @slot('bg', 'yellow-bg')
                                              @slot('btn', 'primary')
                                              @slot('btn_text', '')
                                              @slot('displai', 'display: none;')
                                              @slot('tgl_1', 'Pengumuman')
                                           @else
                                              @slot('bg', 'red-bg')
                                              @slot('btn', 'primary')
                                              @slot('btn_text', '')
                                              @slot('tgl_1', 'Menunggu Pengumuman...')
                                              @slot('displai', 'display: none;')
                                           @endif
                                        @slot('title', 'Menunggu Pengumuman Selanjutnya...')
                                        @slot('text', '')
                                        @slot('tgl_2', date('d-m-Y'))
                                        @slot('route', '')
                                    @endcomponent
                                 </div>
                            </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Info Panel -->
          <div class="info2">
            <div class="col-sm-4 pull-left">
                <div class="panel panel-info">
                    <div class="panel-heading">
                         <i class="fa fa-info-circle"></i> Info
                    </div>
                    <div class="panel-body">
                       <div class="warning"><i class="fa fa-circle text-danger"></i> <span>Password Default Gitlab : <strong class="text-danger">{{ $pass }}</strong></span></div>
                       <div class="warning"><i class="fa fa-circle text-info"></i> <span>Link Group Repository: <a class="text-navy" href="{{ $linkg }}" target="_blank"><b>{{ $linkg }}</b></a></span></div>
                       <div class="warning"><i class="fa fa-circle text-info"></i> <span>Link Login Chat Room: <a class="text-navy" href="{{ $linkm }}" target="_blank"><b>{{ $linkm }}</b></a></span></div>
                    </div>
                </div>
            </div>
          </div>
      </div>
</div>

@stop
@push('scripts')
<!-- Full Calendar -->
<script src="{{ asset('master/js/plugins/fullcalendar/moment.min.js') }}"></script>
<script src="{{ asset('master/js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>

<script>
    $(document).ready(function() {

    });

</script>
@endpush
