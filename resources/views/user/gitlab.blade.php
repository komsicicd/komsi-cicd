@extends('layouts.dashboard')
@extends('user.menu')

@section('title', 'KEJURLAT 2019 | Dashboard User')
@push('styles')
<meta name="csrf" content="{{ csrf_token() }}">
<link href="{{asset('master/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">
<link href="{{asset('master/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{ asset('master/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
<!-- Ladda style -->
<link href="{{ asset('master/css/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">
@endpush
@section('menus')
    <li>
        <a href="{{ route('dashboard.user') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
    </li>
    <li class="active">
        <a href="{{ route('gitlab.user') }}"><i class="fab fa-gitlab"></i> <span class="nav-label">Gitlab & Chat Room</span></a>
    </li>
    <li>
        <a href="{{ route('docker.user') }}"><i class="fab fa-docker"></i> <span class="nav-label">Konfigurasi Docker</span> <span class="pull-right label label-primary" style="display: {{ Auth::user()->progress >= 30 ? 'none' : '' }}">!</span></a>
    </li>
    <li>
        <a href="{{ route('info.user') }}"><i class="fas fa-info-circle"></i> <span class="nav-label">Informasi</span></a>
    </li>
@stop
@section('content')
{{-- @if(Auth::user()->progress < 30)
   <div style="display: {{ Auth::user()->progress < 30 ? '' : 'none' }}">
      <div class="middle-box text-center animated fadeInDown">
         <h1>404</h1>
         <h3 class="font-bold">Halaman Tidak Ditemukan</h3>

         <div class="error-desc">
              Maaf, Halaman yang anda akses tidak kami temukan coba perikas kembali URL yang anda request atau menekan refresh button, atau coba lagi nanti.<br><br>
                  <button type="button" onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Kembali</button>
         </div>
      </div>
   </div>
@else --}}
      <!-- breadcrumb -->
      <div class="row wrapper border-bottom white-bg page-heading">
         <div class="col-lg-8">
              <h2>Konfigurasi Gitlab & Chat Root | Proyek <b class="text-navy">{{ Auth::user()->nama_proyek }}</b></h2>
              <ol class="breadcrumb">
                 <li>
                     <a href="{{ route('dashboard.user') }}">Dashboard</a>
                 </li>
                 <li class="active">
                     <strong>Konfigurasi Gitlab & ChatRoom</strong>
                 </li>
              </ol>
         </div>
         <div class="col-lg-4">
              <div class="title-action animated fadeInRight">
                  <a href="#" class="btn btn-primary toggle_atlit" data-toggle="modal" data-target="#tambah_anggota" style="display: {{ Auth::user()->progress >= 15 ? 'none' : '' }}"><i class="fa fa-plus"></i> Anggota </a>
                  <a href="#" class="btn btn-white" data-toggle="modal" data-target="#tambah_repo" style="display: {{ Auth::user()->progress >= 15 ? 'none' : '' }}"><i class="fa fa-plus"></i> Git Repo </a>
                  <a href="#" id="kunciData" data-anggota="{{ $cang == 0 || Auth::user()->progress >= 15 ? 0 : 1 }}" data-id="{{ Auth::user()->id }}" class="btn btn-warning" {{ $cang == 0 || Auth::user()->progress >= 15 ? 'disabled' : '' }}><i class="fa fa-lock"></i> {{ Auth::user()->progress >= 15 ? 'Terkunci' : 'Kunci Data' }} </a>
              </div>
         </div>
      </div>
         <!-- end breadcrumb -->

         <!-- EDIT Anggota -->
         <div class="modal inmodal" id="editAnggota" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3>Edit Anggota</h3>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{ route('edit.anggota.user') }}" class="edit_anggota_form">
                            @csrf
                            <input type="hidden" name="id" id="aidi">
                            <div class="form-group"><label>Nama Lengkap</label> <input  id="edNama_lengkap" type="text" placeholder="Nama Lengkap" value="{{ old('nama') }}" class="form-control" name="nama_lengkap" autofocus></div>
                            <div class="form-group"><label>Username</label> <input id="edUsername" placeholder="Username" class="form-control" name="username"></div>
                            <div class="form-group"><label>Email</label> <input id="edEmail" type="email" placeholder="Email" class="form-control password_manager" name="email"></div>
                    </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                </div>
            </div>
         </div>
        <!-- END EDIT -->

        <!-- EDIT Repo -->
        <div class="modal inmodal" id="editRepo" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3>Edit Repository</h3>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{ route('edit.repo.user') }}" class="edit_anggota_form">
                            @csrf
                            <input type="hidden" name="id" id="aidiR">
                            <div class="form-group"><label>Konteks</label> <input id="edKonteks" type="text" placeholder="Landing page, WEB API, dst..." class="form-control" name="konteks" autofocus></div>
                            <div class="form-group"><label>Teknologi</label> <input id="edTech" placeholder="Bootstrap, Laravel, React, dst..." class="form-control" name="tech"></div>
                    </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                </div>
            </div>
         </div>
        <!-- END EDIT -->

      <!-- Tambah Repo -->
      <div class="modal inmodal" id="tambah_repo" tabindex="-1" role="dialog"  aria-hidden="true">
         <div class="modal-dialog">
             <div class="modal-content animated fadeIn">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                       <h3>Tambah Repository</h3>
                    </div>
                    <div class="modal-body">
                      <form method="POST" action="{{ route('add.repo.user') }}" class="tambah_repo_form">
                          @csrf
                           <div class="form-group"><label>Konteks</label> <input type="text" placeholder="Landing page, WEB API, dst..." class="form-control" name="konteks" autofocus></div>
                           <div class="form-group"><label>Teknologi</label> <input placeholder="Bootstrap, Laravel, React, dst..." class="form-control" name="tech"></div>
                    </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                             <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                       </form>
               </div>
             </div>
         </div>
      </div>

      <!-- Tambah Anggota -->
      <div class="modal inmodal" id="tambah_anggota" tabindex="-1" role="dialog"  aria-hidden="true">
         <div class="modal-dialog">
             <div class="modal-content animated fadeIn">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                     <h3>Tambah Anggota</h3>
                  </div>
                  <div class="modal-body">
                    <form method="POST" action="{{ route('add.anggota.user') }}" class="tambah_anggota_form">
                        @csrf
                         <div class="form-group"><label>Nama Lengkap</label> <input type="text" placeholder="Nama Lengkap" value="{{ old('nama') }}" class="form-control" name="nama_lengkap" autofocus></div>
                         <div class="form-group"><label>Username</label> <input placeholder="Username" class="form-control" name="username"></div>
                         <div class="form-group"><label>Email</label> <input type="email" placeholder="Email" class="form-control password_manager" name="email"></div>
                  </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                           <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                     </form>
             </div>
         </div>
      </div>
         <!-- End Modal -->


         <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-7">
                    <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Data Anggota<sup class="text-danger">*</sup></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover" id="anggotaTable" >
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Lengkap</th>
                                <th>Username</th>
                                <th>E-mail</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>

                    </div>
                    </div>
         </div>
         <div class="col-lg-5">
            <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Data Repository<sup class="text-danger">*</sup></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
            <table class="table table-striped table-bordered table-hover" id="repoTable" >
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Konteks</th>
                        <th>Teknologi</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <div id="MessageContainer"></div>
            </table>

            </div>
            </div>
        </div>
      </div>

      <div class="row">
        <!-- Info Panel -->
          <div class="info2">
            <div class="col-sm-3 pull-left">
                <div class="panel panel-info">
                    <div class="panel-heading">
                         <i class="fa fa-info-circle"></i> Info
                    </div>
                    <div class="panel-body">
                       <div class="warning"><i class="fa fa-circle text-danger"></i> <span>Password Gitlab : <strong class="text-danger">{{ $pass }}</strong></span></div>
                       <div class="danger"><i class="fa fa-circle text-danger"></i> <span><sup class="text-danger">* </sup>Wajib diisi</span></div>
                       <div class="danger"><i class="fa fa-circle text-warning"></i> <span>Pastikan kelengkapan data sebelum kunci data!</span></div>
                       <div class="danger"><i class="fa fa-circle text-warning"></i> <span>Jika terdapat kesalahan input atau pengajuan tambah data,</span></div>
                       <div class="warning"><i class="fa fa-circle text-white"></i> <span>silahkan isi form di tab pengajuan</span></div>
                       <div class="warning"><i class="fa fa-circle text-info"></i> <span>Ubah Password gitlab setelah pertama kali login!</span></div>
                       <div class="warning"><i class="fa fa-circle text-info"></i> <span>Credential Gitlab dapat dipakai setelah di konfirmasi oleh Admin</span></div>
                    </div>
                </div>
            </div>
          </div>
      </div>

    </div>

{{-- @endif --}}
@stop

@push('scripts')
@if(Session::has('anggota'))
<script>
   $(document).ready(function() {
       toastr.options = {
           closeButton: true,
           progressBar: true,
           preventDuplicates: true,
           positionClass: 'toast-top-right',
       };
       toastr.success('{{ Session::get('anggota') }}', 'Berhasil!');

   });
</script>
@endif
<!-- Jquery Validate -->
<script src="{{ asset('master/js/plugins/validate/jquery.validate.min.js') }}"></script>

<!-- SWAL -->
<script src="{{ asset('master/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script>
    $(document).ready(function(){

    //kunci Data
    $(document).on('click', '#kunciData', function () {
         var user_id = $(this).data('id');
         if ($(this).data('anggota')) {
            swal({
                       title: "Anda Yakin?",
                       text: "Data yang sudah dikunci tidak dapat dikembalikan lagi!",
                       type: "warning",
                       showCancelButton: true,
                       confirmButtonColor: "#DD6B55",
                       confirmButtonText: "Ya, Kunci!",
                       cancelButtonText: "Tidak!",
                       closeOnConfirm: false,
                       closeOnCancel: false },
                   function (isConfirm) {
                       if (isConfirm) {
                         $.ajax({
                            url: "{{ route('kunci.anggota.user') }}",
                            method: "POST",
                            headers: {
                              "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                            },
                            data: { id: user_id },
                            success: function(){
                                 swal("DiKunci!", "Data berhasil dikunci!.", "success");
                                 setTimeout(function(){
                                    window.location.href = "{{ route('dashboard.user') }}";
                                 }, 1500);
                            },
                            error: function(e){
                               console.error(e);
                               swal("Error", "Hubungi Administrator untuk Solusi Lebih lanjut.", "error");
                            }
                         });
                       } else {
                           swal("Dibatalkan", "Data Tidak Jadi Dikunci", "error");
                       }
                   });
         }
      });
      // END KUNCI DATA

    //Edit Anggota
    $(document).on('click', '.editRepo', function () {
         var ang_id = $(this).data('id');
               $.ajax({
                  url: "{{ route('dataEdit.repo.user') }}",
                  method: "POST",
                  headers: {
                     "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                  },
                  data: { id: ang_id },
                  success: function(data){
                     $('#aidiR').val(ang_id);
                     $('#edKonteks').val(data[0].konteks);
                     $('#edTech').val(data[0].tech);
                  },
                  error: function(){
                     swal("ERROR", "Terjadi kesalahan pada saat mengupdate data!", "error");
                  }
               });
      });
      //END EDIT

    //Delete Repo
    $(document).on('click', '.deleteRepo', function () {
         var ang_id = $(this).data('id');
          swal({
                      title: "Anda Yakin?",
                      text: "Data yang sudah dihapus tidak dapat dikembalikan lagi!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Ya, Hapus!",
                      cancelButtonText: "Tidak!",
                      closeOnConfirm: false,
                      closeOnCancel: false },
                  function (isConfirm) {
                      if (isConfirm) {
                           $.ajax({
                              url: "{{ route('del.repo.user') }}",
                              method: "POST",
                              headers: {
                                 "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                              },
                              data: { id: ang_id },
                              success: function(){
                                 swal("Dihapus!", "Data yang anda pilih berhasil dihapus!.", "success");
                                 setTimeout(function(){
                                    window.location.reload();
                                 }, 1500);
                              },
                              error: function(){
                                 swal("ERROR", "Terjadi kesalahan pada saat menghapus data!", "error");
                              }
                           });
                      } else {
                          swal("Dibatalkan", "Data yang anda pilih Tidak Jadi Dihapus", "error");
                      }
                  });
            });
        // END DELETE

    //Edit Anggota
    $(document).on('click', '.editAnggota', function () {
         var ang_id = $(this).data('id');
               $.ajax({
                  url: "{{ route('dataEdit.anggota.user') }}",
                  method: "POST",
                  headers: {
                     "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                  },
                  data: { id: ang_id },
                  success: function(data){
                     $('#aidi').val(ang_id);
                     $('#edNama_lengkap').val(data[0].nama_lengkap);
                     $('#edUsername').val(data[0].username);
                     $('#edEmail').val(data[0].email);
                  },
                  error: function(){
                     swal("ERROR", "Terjadi kesalahan pada saat menghapus data!", "error");
                  }
               });
      });
      //END EDIT

    //Delete Anggota
      $(document).on('click', '.deleteAnggota', function () {
         var ang_id = $(this).data('id');
          swal({
                      title: "Anda Yakin?",
                      text: "Data yang sudah dihapus tidak dapat dikembalikan lagi!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Ya, Hapus!",
                      cancelButtonText: "Tidak!",
                      closeOnConfirm: false,
                      closeOnCancel: false },
                  function (isConfirm) {
                      if (isConfirm) {
                           $.ajax({
                              url: "{{ route('del.anggota.user') }}",
                              method: "POST",
                              headers: {
                                 "X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')
                              },
                              data: { id: ang_id },
                              success: function(){
                                 swal("Dihapus!", "Data yang anda pilih berhasil dihapus!.", "success");
                                 setTimeout(function(){
                                    window.location.reload();
                                 }, 1500);
                              },
                              error: function(){
                                 swal("ERROR", "Terjadi kesalahan pada saat menghapus data!", "error");
                              }
                           });
                      } else {
                          swal("Dibatalkan", "Data yang anda pilih Tidak Jadi Dihapus", "error");
                      }
                  });
            });
        // END DELETE

        // GET DATA
            $('#anggotaTable').DataTable({
                columnDefs: [{
                   defaultContent: "-",
                   targets: "_all"
                }],
                // dom: '<"html5buttons"B>lTfgitp',
                // buttons: [
                //     {extend: 'copy'},
                //     {extend: 'csv'},
                //     {extend: 'excel', title: 'ExampleFile'},
                //     {extend: 'pdf', title: 'ExampleFile'},

                //     {extend: 'print',
                //      customize: function (win){
                //             $(win.document.body).addClass('white-bg');
                //             $(win.document.body).css('font-size', '10px');

                //             $(win.document.body).find('table')
                //                     .addClass('compact')
                //                     .css('font-size', 'inherit');
                //     }
                //     }
                // ],
                responsive: true,
                prossessing: true,
                serverSide: true,
                ajax: '{!! route('data.anggota.user') !!}',
                columns: [
                  { name: '', data: 'DT_RowIndex' },
                  { name: 'nama_lengkap', data: 'nama_lengkap' },
                  { name: 'username', data: 'username' },
                  { name: 'email', data: 'email' },
                  {
                     name: 'action',
                     data: 'action',
                     sortable: false
                  },
               ]
            });

            $('#repoTable')
            .on( 'draw.dt', function () {
                console.log( 'Loading' );
                $("#MessageContainer").html('<img class="center" src="https://i.pinimg.com/originals/3f/2c/97/3f2c979b214d06e9caab8ba8326864f3.gif">')
            } )
            .on( 'init.dt', function () {
                console.log( 'Loaded' );
                $("#MessageContainer").html("")
            } )
            .DataTable({
                columnDefs: [{
                   defaultContent: "-",
                   targets: "_all"
                }],
                responsive: true,
                prossessing: true,
                serverSide: true,
                ajax: '{!! route('data.repo.user') !!}',
                columns: [
                  { name: '', data: 'DT_RowIndex' },
                  { name: 'konteks', data: 'konteks' },
                  { name: 'tech', data: 'tech' },
                  {
                     name: 'action',
                     data: 'action',
                     sortable: false
                  },
               ]
            });
        });
</script>
@endpush
