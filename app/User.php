<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_ketua', 'role', 'nama_proyek', 'email', 'no_telp', 'password', 'progress', 'nama_pembimbing', 'stagingLink'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function anggota()
    {
      return $this->hasMany(\App\Anggota::class);
    }

    public function repo()
    {
      return $this->hasMany('App\Repo');
    }

    public function notif()
    {
      return $this->hasMany('App\Notif');
    }
}
