<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    protected $fillable = [
        'nama_lengkap', 'username', 'email', 'user_id'
    ];

    public function user()
    {
      return $this->belongsTo(\App\User::class);
    }
}
