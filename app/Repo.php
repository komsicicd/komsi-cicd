<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repo extends Model
{
    protected $fillable = [
        'konteks', 'tech', 'user_id', 'link', 'dockerImage'
    ];
}
