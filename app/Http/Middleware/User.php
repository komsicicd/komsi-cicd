<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role == 1) {
            return redirect()->route('dashboard.admin');
        }
        if (Auth::user()->role == 0) {
            return redirect()->route('verify.user');
        }
         // session()->flash('admin', 'Hanya Administrator yang dapat Mengakses Halaman Tersebut!');
         return $next($request);
    }
}
