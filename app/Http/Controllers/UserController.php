<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\DataTables;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('user.dashboard');
    }

    public function gitlab()
    {
        $id = Auth::user()->id;
        $sen = Auth::user()->nama_proyek . Auth::user()->id;
        $pass = str_replace(' ', '', $sen);
        $pass = preg_replace('/\s/', '', $sen);
        $pass = "KOMSI" . strtolower($pass);
        $anggota = \App\Anggota::where('user_id', $id)->get();
        if($anggota){
            $cang = count($anggota);
        }else {
            $cang = 0;
        }
        return view('user.gitlab', compact(['anggota', 'cang', 'pass']));
    }

    public function verify()
    {
        return view('user.verify');
    }

    public function getAnggota(Request $req)
    {
        if($req->ajax()){
            $data = \App\Anggota::where('user_id', Auth::user()->id)->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        if(Auth::user()->progress >= 15){
                            $btn = '<button class="btn btn-info btn-circle btn-outline editAnggota" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#editAnggota" disabled><i class="fas fa-paste"></i></button> <button class="btn btn-danger btn-circle deleteAnggota" data-id="'. $data['id'] .'" disabled><i class="fa fa-trash" ></i></button>';
                        } else {
                            $btn = '<button class="btn btn-info btn-circle btn-outline editAnggota" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#editAnggota"><i class="fas fa-paste"></i></button> <button class="btn btn-danger btn-circle deleteAnggota" data-id="'. $data['id'] .'"><i class="fa fa-trash" ></i></button>';
                        }
                        return $btn;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    // Delete Anggota
   public function delAnggota(Request $req)
   {
      \App\Anggota::destroy($req->id);
   }

   // Tambah Anggota
   public function tambahAnggota(Request $req)
   {
      $atlit = \App\Anggota::create([
         'nama_lengkap' => $req->nama_lengkap,
         'username' => $req->username,
         'email' => $req->email,
         'user_id' => Auth::user()->id
      ]);
      session()->flash('anggota', 'Anggota Berhasil Ditambahkan!');
      return redirect()->back();
   }

   // Data Edit Anggota
   public function dataEditAnggota(Request $req)
   {
      $ang = \App\Anggota::where('user_id', Auth::user()->id)->find($req->id);
      return response()->json([
        $ang
     ], 200);
   }

   // Edit Anggota
   public function editAnggota(Request $req)
   {
      $id = $req->id;
      $anggota = \App\Anggota::find($id);
      $anggota->update([
         'nama_lengkap' => $req->nama_lengkap,
         'username' => $req->username,
         'email' => $req->email,
         'user_id' => Auth::user()->id
      ]);
      session()->flash('anggota', 'Anggota Berhasil Diupdate!');
      return redirect()->back();
   }

   // Get Data Repo
   public function getRepo(Request $req)
    {
        if($req->ajax()){
            $data = \App\Repo::where('user_id', Auth::user()->id)->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        if(Auth::user()->progress >= 15){
                            $btn = '<button class="btn btn-info btn-circle btn-outline editRepo" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#editRepo" disabled><i class="fas fa-paste"></i></button> <button class="btn btn-danger btn-circle btn-sm deleteRepo" data-id="'. $data['id'] .'" disabled><i class="fa fa-trash" ></i></button>';
                        }else {
                            $btn = '<button class="btn btn-info btn-circle btn-outline editRepo" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#editRepo"><i class="fas fa-paste"></i></button> <button class="btn btn-danger btn-circle btn-sm deleteRepo" data-id="'. $data['id'] .'"><i class="fa fa-trash" ></i></button>';
                        }
                        return $btn;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    // Tambah Repo
   public function tambahRepo(Request $req)
   {
      $atlit = \App\Repo::create([
         'konteks' => $req->konteks,
         'tech' => $req->tech,
         'user_id' => Auth::user()->id
      ]);
      session()->flash('anggota', 'Repo Berhasil Ditambahkan!');
      return redirect()->back();
   }

    // Delete Repo
    public function delRepo(Request $req)
    {
       \App\Repo::destroy($req->id);
    }

    // Data Edit Repo
   public function dataEditRepo(Request $req)
   {
      $ang = \App\Repo::where('user_id', Auth::user()->id)->find($req->id);
      return response()->json([
        $ang
     ], 200);
   }

   // Edit Repo
   public function editRepo(Request $req)
   {
      $id = $req->id;
      $anggota = \App\Repo::find($id);
      $anggota->update([
         'konteks' => $req->konteks,
         'tech' => $req->tech,
         'user_id' => Auth::user()->id
      ]);
      session()->flash('anggota', 'Repo Berhasil Diupdate!');
      return redirect()->back();
   }

    // Kunci Anggota & Repo
    public function kunciAnggota(Request $req)
    {
        $id = $req->id;
        $user = \App\User::find($id);
        $user->progress = 15;
        $user->save();
        \App\Notif::create([
            'status' => 'Menunggu Konfirmasi',
            'is_config' => false,
            'label' => 'warning',
            'konten' => 'Akun Gitlab yang anda daftarkan sedang di Buat dan di konfirmasi',
            'cta' => '#',
            'toggle' => '#',
            'progress' => $user->progress,
            'user_id' => $id
        ]);
    }

    // Informasi View
    public function info()
    {
        $i = 1;
        $ia = 1;
        $ang = \App\User::find(Auth::user()->id);
        $anggota = \App\Anggota::where('user_id', Auth::user()->id)->get();
        $active = $ang->progress > 15 ? 'Aktif' : 'Belum Aktif';
        $label = $ang->progress > 15 ? 'primary' : 'warning';
        $sen = Auth::user()->nama_proyek . Auth::user()->id;
        $pass = str_replace(' ', '', $sen);
        $pass = preg_replace('/\s/', '', $sen);
        $pass = "KOMSI" . strtolower($pass);
        $sen2 = Auth::user()->nama_proyek;
        $link = str_replace(' ', '', $sen2);
        $link = preg_replace('/\s/', '', $sen2);
        $linkg = 'https://git.komsicicd.site/' . strtolower($link);
        $linkm = 'https://chat.komsicicd.site/' . strtolower($link);
        $repo = \App\Repo::where('user_id', Auth::user()->id)->get();
        $notif = \App\Notif::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        $komentar = \App\Komentar::where([['user_id', '=', Auth::user()->id], ['is_admin', '=', 1]])->get();
        return view('user.info', compact(['komentar', 'ang', 'anggota', 'i', 'ia', 'label', 'active', 'pass', 'repo', 'linkg', 'linkm', 'notif']));
    }

    // Docker Config
    public function docker()
    {
        $i = 1;
        $sen = Auth::user()->nama_proyek;
        $np = str_replace(' ', '', $sen);
        $np = preg_replace('/\s/', '', $sen);
        $np = strtolower($np);
        $scmsg = 'Konfigurasi Docker & Dockerfile telah terkonfirmasi';
        $repo = \App\Repo::where('user_id', Auth::user()->id)->get();
        return view('user.docker', compact(['np', 'scmsg', 'repo', 'i']));
    }

    // Docker Android
    public function dockerAndroid(Request $req)
    {
        $id = Auth::user()->id;
        $user = \App\User::find($id);
        if($req->android == 'option1'){
            $user->progress = 80;
            $user->save();
            \App\Notif::create([
                'status' => 'Selesai',
                'is_config' => false,
                'label' => 'success',
                'konten' => 'Selamat! proyek anda telah menyelesaikan semuanya, Tunggu Pemberitahuan selanjutnya',
                'cta' => '#',
                'toggle' => '#',
                'progress' => $user->progress,
                'user_id' => $user->id
            ]);
            return redirect()->route('dashboard.user');
        }
        $user->progress = 45;
        $user->save();
        \App\Notif::create([
            'status' => 'Docker',
            'is_config' => true,
            'label' => 'success',
            'konten' => 'Segera Selesaikan Konfigurasi',
            'cta' => route('docker.user'),
            'cta_text' => 'Konfigurasi',
            'cta_icon' => 'fa fa-tools',
            'toggle' => '#',
            'progress' => $user->progress,
            'user_id' => $user->id
        ]);

        return redirect()->back();
    }

    // Docker Image
    public function dockerImage(Request $req)
    {
        $id = $req->id;
        $repo = \App\Repo::find($id);
        $repo->dockerImage = $req->dockerImage;
        $repo->save();
        return redirect()->back();
    }

    // Docker Done
    public function dockerDone(Request $req)
    {
        $link = explode(',', $req->imageLink);
        $id = Auth::user()->id;
        $user = \App\User::find($id);
        $user->progress = $user->progress + $req->prog;
        $user->save();
        \App\Notif::create([
            'status' => 'Menunggu Konfirmasi',
            'is_config' => false,
            'label' => 'warning',
            'konten' => 'Docker Image yang anda push ke Registry sedang di periksa dan dikonfirmasi',
            'cta' => '#',
            'toggle' => '#',
            'progress' => $user->progress,
            'user_id' => $user->id
        ]);
    }
}
