<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Notif;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama_ketua' => ['required', 'string', 'max:255'],
            'nama_proyek' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'no_telp' => ['required', 'numeric', 'digits_between:5,12', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
                'nama_ketua' => $data['nama_ketua'],
                'nama_proyek' => $data['nama_proyek'],
                'nama_pembimbing' => $data['nama_pembimbing'],
                'email' => $data['email'],
                'no_telp' => $data['no_telp'],
                'password' => Hash::make($data['password']),
            ]);
    }

    protected function notif()
    {
        $id = \App\User::select('id')->orderBy('id', 'DESC')->take(1)->first();
        $user_id = $id->id + 1;
        Notif::create([
            'status' => 'Not Verified',
            'is_config' => false,
            'label' => 'danger',
            'konten' => 'Tunggu konfigurasi dari admin!',
            'cta' => '',
            'cta_text' => '',
            'cta_icon' => '',
            'toggle' => '',
            'progress' => 0,
            'user_id' => $user_id
        ]);
    }

}
