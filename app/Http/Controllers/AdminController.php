<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\DataTables;
use Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelompok = \App\User::all();
        $anggota = \App\Anggota::all();
        $repo = \App\Repo::all();
        $docker = \App\Repo::where('dockerImage', '!=', '-')->orWhereNull('dockerImage')->get();
        $tkel = count($kelompok);
        $tang = count($anggota);
        $trep = count($repo);
        $tdoc = count($docker);

        $notif = \App\Notif::orderBy('updated_at', 'DESC')->take(10)->get();
        return view('admin.dashboard', compact(['notif', 'tkel', 'tang', 'trep', 'tdoc']));
    }

    // Verifikasi Proyek
    public function verify()
    {
        return view('admin.verify');
    }

    // Data Pendaftaran Proyek
    public function pp(Request $req)
    {
        if($req->ajax()){
            $data = \App\User::where('role', 0)->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        return '<button class="btn btn-info btn-circle btn-outline editProyek" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#editProyek"><i class="fas fa-paste"></i></button> <button class="btn btn-danger btn-circle deleteProyek" data-id="'. $data['id'] .'"><i class="fa fa-trash" ></i></button> <button class="btn btn-primary btn-circle cProyek" data-id="'. $data['id'] .'"><i class="fa fa-check" ></i></button>';
                    })
                    ->addColumn('no_telp', function($data){
                        return '<a class="text-navy" href="https://wa.me/+62'. $data['no_telp'] .'" target="_blank"><i class="fab fa-whatsapp"></i> 0'. $data['no_telp'] .'</a>';
                    })
                    ->addColumn('pass', function($data){
                        $sen = $data->nama_proyek . $data->id;
                        $pass = str_replace(' ', '', $sen);
                        $pass = preg_replace('/\s/', '', $sen);
                        $pass = "KOMSI" . strtolower($pass);
                        return '<strong class="text-danger">'. $pass .'</strong>';
                    })
                    ->addColumn('progress', function($data){
                        $label = '<span class="label label-danger">Not Verified</span>';
                        return $label;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['action', 'no_telp', 'pass', 'progress'])
                    ->make(true);
        }
    }

    // Data Proyek Terdaftar
    public function pt(Request $req)
    {
        if($req->ajax()){
            $data = \App\User::where('role', 2)->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        return '<button class="btn btn-info btn-circle btn-outline editProyek" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#editProyek"><i class="fas fa-paste"></i></button> <button class="btn btn-danger btn-circle deleteProyek" data-id="'. $data['id'] .'"><i class="fa fa-trash" ></i></button> <button class="btn btn-warning btn-circle ucProyek" data-id="'. $data['id'] .'"><i class="fa fa-times" ></i></button>';
                    })
                    ->addColumn('no_telp', function($data){
                        return '<a class="text-navy" href="https://wa.me/+62'. $data['no_telp'] .'" target="_blank"><i class="fab fa-whatsapp"></i> 0'. $data['no_telp'] .'</a>';
                    })
                    ->addColumn('pass', function($data){
                        $sen = $data->nama_proyek . $data->id;
                        $pass = str_replace(' ', '', $sen);
                        $pass = preg_replace('/\s/', '', $sen);
                        $pass = "KOMSI" . strtolower($pass);
                        return '<strong class="text-danger">'. $pass .'</strong>';
                    })
                    ->addColumn('progress', function($data){
                        if($data['progress'] == 0){
                            $label = '<span class="label label-primary">Verified</span>';
                        }
                        elseif($data['progress'] == 15){
                            $label = '<span class="label label-warning">Confirm Gitlab</span>';
                        }
                        elseif($data['progress'] == 30){
                            $label = '<span class="label label-info">Docker/Android</span>';
                        }
                        elseif($data['progress'] == 45){
                            $label = '<span class="label label-info">Docker Config</span>';
                        }
                        elseif($data['progress'] == 60){
                            $label = '<span class="label label-warning">Confirm Docker</span>';
                        }
                        elseif($data['progress'] == 80){
                            $label = '<span class="label label-success">Done</span>';
                        }
                        else {
                            $label = '<span class="label label-danger">Not Verified</span>';
                        }
                        return $label;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['action', 'no_telp', 'pass', 'progress'])
                    ->make(true);
        }
    }

    // Konfirmasi Proyek
    public function confirm(Request $req)
    {
        $id = $req->id;
        $user = \App\User::find($id);
        $user->role = 2;
        $user->save();
        \App\Notif::create([
            'status' => 'Verified',
            'is_config' => true,
            'label' => 'success',
            'konten' => 'Lengkapi Konfigurasi Gitlab & Chat Room Sekarang!',
            'cta' => route('gitlab.user'),
            'cta_text' => 'Konfigurasi',
            'cta_icon' => 'fa fa-tools',
            'toggle' => '',
            'progress' => $user->progress,
            'user_id' => $user->id
        ]);
    }

    // Batal Konfirmasi Proyek
    public function unconfirm(Request $req)
    {
        $id = $req->id;
        $user = \App\User::find($id);
        $user->role = 0;
        $user->save();
        \App\Notif::create([
            'status' => 'Not Verified',
            'is_config' => false,
            'label' => 'danger',
            'konten' => 'Tunggu konfigurasi dari admin!',
            'cta' => '',
            'cta_text' => '',
            'cta_icon' => '',
            'toggle' => '',
            'progress' => $user->progress,
            'user_id' => $user->id
        ]);
    }

    // Delete Proyek
    public function delProyek(Request$req)
    {
        \App\User::destroy($req->id);
    }

    // Data Edit Proyek
   public function dataEditProyek(Request $req)
   {
      $ang = \App\User::find($req->id);
      $data = [
        'nama_proyek' => $ang->nama_proyek,
        'nama_ketua' => $ang->nama_ketua,
        'nama_pembimbing' => $ang->nama_pembimbing,
        'email' => $ang->email,
        'no_telp' => $ang->no_telp,
        'progress' => $ang->progress
      ];
      return response()->json([
        $data
     ], 200);
   }

   // Edit Proyek
   public function editProyek(Request $req)
   {
      $id = $req->id;
      $anggota = \App\User::find($id);
      $anggota->update([
        'nama_ketua' => $req->nama_ketua,
        'nama_proyek' => $req->nama_proyek,
        'nama_pembimbing' => $req->nama_pembimbing,
        'email' => $req->email,
        'no_telp' => $req->no_telp,
        'password' => bcrypt($req->password),
        'progress' => $req->progress,
      ]);
      session()->flash('anggota', 'Proyek Berhasil Diupdate!');
      return redirect()->back();
   }

   // Tambah Proyek
   public function addProyek(Request $req)
   {
      $atlit = \App\User::create([
        'nama_ketua' => $req->nama_ketua,
        'nama_proyek' => $req->nama_proyek,
        'nama_pembimbing' => $req->nama_pembimbing,
        'email' => $req->email,
        'no_telp' => $req->no_telp,
        'password' => bcrypt($req->password),
      ]);
      session()->flash('anggota', 'Proyek Berhasil Ditambahkan!');
      return redirect()->back();
   }

   // Gitlab View
   public function gitlab()
   {
       return view('admin.gitlab');
   }

   // Gitlab Detail
   public function gitlabDetail($id)
   {
        $i = 1;
        $a = 1;
        $user = \App\User::find($id);
        $repo = \App\Repo::where('user_id', $id)->get();
        $sen = $user->nama_proyek . $id;
        $pass = str_replace(' ', '', $sen);
        $pass = preg_replace('/\s/', '', $sen);
        $pass = "KOMSI" . strtolower($pass);
        return view('admin.gitlabDeteil', compact(['user', 'pass', 'repo', 'i', 'a']));
   }

   // Data Gitlab
   public function dataGitlab(Request $req)
   {
        if($req->ajax()){
            $data = \App\User::where('progress', 15)->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        return '<a href="'. route('detail.gitlab.admin', ['id' => $data['id']]) .'" class="btn btn-success btn-circle"><i class="fas fa-eye"></i></a> <button class="btn btn-primary btn-circle cGitlab" data-id="'. $data['id'] .'"><i class="fa fa-check" ></i></button>';
                    })
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
        }
   }

   // Data Confirmed Gitlab
   public function dataConfirmGitlab(Request $req)
   {
        if($req->ajax()){
            $data = \App\User::where('progress', 30)->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        return '<a href="'. route('detail.gitlab.admin', ['id' => $data['id']]) .'" class="btn btn-success btn-circle"><i class="fas fa-eye"></i></a> <button class="btn btn-warning btn-circle ucGitlab" data-id="'. $data['id'] .'"><i class="fa fa-times" ></i></button>';
                    })
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
        }
   }

   // Data Gitlab Detail
   public function dataGitlabDetail(Request $req, $id)
   {
        if($req->ajax()){
            $data = \App\Anggota::where('user_id', $id)->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        $btn = '<button class="btn btn-info btn-circle btn-outline editAnggota" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#editAnggota"><i class="fas fa-paste"></i></button> <button class="btn btn-danger btn-circle deleteAnggota" data-id="'. $data['id'] .'"><i class="fa fa-trash" ></i></button>';
                        return $btn;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
        }
   }

   // Gitlab Tambah Anggota
   public function tambahAnggota(Request $req, $user_id)
   {
      $atlit = \App\Anggota::create([
         'nama_lengkap' => $req->nama_lengkap,
         'username' => $req->username,
         'email' => $req->email,
         'user_id' => $user_id
      ]);
      session()->flash('anggota', 'Anggota Berhasil Ditambahkan!');
      return redirect()->back();
   }

   // Gitlab Delete Anggota
   public function delAnggota(Request $req)
   {
      \App\Anggota::destroy($req->id);
   }

   // Gitlab Data Edit Anggota
   public function dataEditAnggota(Request $req, $user_id)
   {
      $ang = \App\Anggota::where('user_id', $user_id)->find($req->id);
      return response()->json([
        $ang
     ], 200);
   }

   // Gitlab Edit Anggota
   public function editAnggota(Request $req, $user_id)
   {
      $id = $req->id;
      $anggota = \App\Anggota::find($id);
      $anggota->update([
         'nama_lengkap' => $req->nama_lengkap,
         'username' => $req->username,
         'email' => $req->email,
         'user_id' => $user_id
      ]);
      session()->flash('anggota', 'Anggota Berhasil Diupdate!');
      return redirect()->back();
   }

   // Data Gitlab Repo Detail
   public function dataGitlabRepoDetail(Request $req, $id)
   {
        if($req->ajax()){
            $data = \App\Repo::where('user_id', $id)->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        $btn = '<button class="btn btn-info btn-circle btn-outline editRepo" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#editRepo"><i class="fas fa-paste"></i></button> <button class="btn btn-danger btn-circle btn-sm deleteRepo" data-id="'. $data['id'] .'"><i class="fa fa-trash" ></i></button>';
                        return $btn;
                    })
                    ->addColumn('link', function($data){
                        if($data['link'] == '-'){
                            $col = '<a href="#" class="btn btn-warning linkRepo" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#linkRepo"><i class="fa fa-plus"> Link</i></a>';
                        } else {
                            $col = '<span class="btn btn-primary btn-circle linkRepo" data-id="'. $data['id'] .'"><i class="fa fa-check"></i></span>';
                        }

                        return $col;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['action', 'link'])
                    ->make(true);
        }
   }

    // Gitlab Tambah Repo
    public function tambahRepo(Request $req, $user_id)
    {
       $atlit = \App\Repo::create([
          'konteks' => $req->konteks,
          'tech' => $req->tech,
          'user_id' => $user_id
       ]);
       session()->flash('anggota', 'Repo Berhasil Ditambahkan!');
       return redirect()->back();
    }

    // Gitlab Delete Repo
    public function delRepo(Request $req)
    {
       \App\Repo::destroy($req->id);
    }

    // Gitlab Data Edit Repo
   public function dataEditRepo(Request $req, $user_id)
   {
      $ang = \App\Repo::where('user_id', $user_id)->find($req->id);
      return response()->json([
        $ang
     ], 200);
   }

   // Gitlab Edit Repo
   public function editRepo(Request $req, $user_id)
   {
      $id = $req->id;
      $anggota = \App\Repo::find($id);
      $anggota->update([
         'konteks' => $req->konteks,
         'tech' => $req->tech,
         'user_id' => $user_id
      ]);
      session()->flash('anggota', 'Repo Berhasil Diupdate!');
      return redirect()->back();
   }

   // Konfirmasi Gitlab
   public function confirmGitlab(Request $req)
   {
       $id = $req->id;
       $user = \App\User::find($id);
       $user->progress = 30;
       $user->save();
       \App\Notif::create([
            'status' => 'Dikonfirmasi',
            'is_config' => true,
            'label' => 'primary',
            'konten' => 'Akun Gitlab yang anda daftarkan telah di Buat dan terkonfirmasi',
            'cta' => '#tab-1',
            'cta_text' => 'Lihat',
            'cta_icon' => 'fas fa-eye',
            'toggle' => 'tab',
            'progress' => $user->progress,
            'user_id' => $user->id
        ]);
   }

   // Batal Konfirmasi Gitlab
   public function unconfirmGitlab(Request $req)
   {
       $id = $req->id;
       $user = \App\User::find($id);
       $user->progress = 15;
       $user->save();
        \App\Notif::create([
            'status' => 'Ditolak',
            'is_config' => true,
            'label' => 'danger',
            'konten' => 'Akun Gitlab yang anda daftarkan ditolak atau batal di konfirmasi',
            'cta' => '#tab-1',
            'cta_text' => 'Lihat',
            'cta_icon' => 'fas fa-eye',
            'toggle' => 'tab',
            'progress' => $user->progress,
            'user_id' => $user->id
        ]);
   }

   // Link Repo
   public function linkRepo(Request $req, $user_id)
   {
        $id = $req->id;
        $repo = \App\Repo::find($id);
        $repo->link = $req->link;
        $repo->save();
        // dd();
        return redirect()->back();
   }

   // Docker
   public function docker()
   {
       return view('admin.docker');
   }

   // Data Docker
   public function dataDocker(Request $req)
   {
        if($req->ajax()){
            $data = \App\User::where('progress', 60)->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        return '<a href="#" class="btn btn-success btn-circle detailDocker" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#detailDocker"><i class="fas fa-eye"></i></a> <button class="btn btn-primary btn-circle cDocker" data-id="'. $data['id'] .'"><i class="fa fa-check" ></i></button> <button class="btn btn-warning btn-circle komentarDocker" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#commentDocker"><i class="fas fa-comments" ></i></button>';
                    })
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
        }
   }

   // Data Confirmed Docker
   public function dataConfirmedDocker(Request $req)
   {
        if($req->ajax()){
            $data = \App\User::where('progress', 80)->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        return '<a href="#" class="btn btn-success btn-circle detailDocker" data-id="'. $data['id'] .'" data-toggle="modal" data-target="#detailDocker"><i class="fas fa-eye"></i></a> <button class="btn btn-warning btn-circle ucDocker" data-id="'. $data['id'] .'"><i class="fa fa-times" ></i></button>';
                    })
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
        }
   }

   // Data Detail Docker
   public function dataDetailDocker(Request $req)
   {
        if($req->ajax()){
            $id = $req->id;
            $repo = \App\Repo::where('user_id', $id)->get();
            return $repo;
        }
   }

   // Confirm Docker
   public function confirmDocker(Request $req)
   {
        $id = $req->id;
        $user = \App\User::find($id);
        $user->progress = 80;
        $user->save();
        \App\Notif::create([
            'status' => 'Dikonfirmasi',
            'is_config' => true,
            'label' => 'primary',
            'konten' => 'Docker Image yang anda push ke Registry telah terkonfirmasi',
            'cta' => '#tab-3',
            'cta_text' => 'Lihat',
            'cta_icon' => 'fas fa-eye',
            'toggle' => 'tab',
            'progress' => $user->progress,
            'user_id' => $user->id
        ]);
        return back();
   }

   // UnConfirm Docker
   public function unconfirmDocker(Request $req)
   {
        $id = $req->id;
        $user = \App\User::find($id);
        $user->progress = 60;
        $user->save();
        \App\Notif::create([
            'status' => 'Ditolak',
            'is_config' => true,
            'label' => 'danger',
            'konten' => 'Docker Image yang anda push ke Registry ditolak atau batal di konfirmasi',
            'cta' => '#tab-3',
            'cta_text' => 'Lihat',
            'cta_icon' => 'fas fa-eye',
            'toggle' => 'tab',
            'progress' => $user->progress,
            'user_id' => $user->id
        ]);
        return back();
   }

   // Info View
   public function info(Request $req)
   {
       return view('admin.info');
   }

   // Komentar Docker
   public function komentar(Request $req)
   {
    $komentar = \App\Komentar::create([
        'komentar' => $req->komentar,
        'is_admin' => $req->is_admin,
        'user_id' => $req->user_id,
      ]);
      $id = $req->user_id;
      $user = \App\User::find($id);
      $user->progress = 30;
      $user->save();
      \App\Notif::create([
        'status' => 'Direvisi',
        'is_config' => true,
        'label' => 'info',
        'konten' => 'Ada revisi yang harus kamu perbaiki!',
        'cta' => '#tab-5',
        'cta_text' => 'Lihat',
        'cta_icon' => 'fas fa-eye',
        'toggle' => 'tab',
        'progress' => $user->progress,
        'user_id' => $user->id
    ]);
      session()->flash('anggota', 'Komentar/Revisi berhasi dikirim!');
      return redirect()->back();
   }
}

