<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class RedirectUserController extends Controller
{
   public function __construct()
   {
       $this->middleware('auth');
   }

    public function redirect()
    {
        if (Auth::user()->role == 1) {
            session()->flash('msg', 'Selamat Datang ' . Auth::user()->nama_ketua);
            return redirect()->route('dashboard.admin');
        }
        else if(Auth::user()->role == 2) {
            session()->flash('msg', 'Selamat Datang ' . Auth::user()->nama_ketua);
            return redirect()->route('dashboard.user');
        }
        else {
            return redirect()->route('verify.user');
        }
    }
}
