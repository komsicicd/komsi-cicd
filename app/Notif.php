<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notif extends Model
{
    protected $fillable = [
        'status', 'konten', 'cta', 'cta_text', 'cta_icon', 'toggle', 'user_id', 'label', 'is_config', 'progress'
    ];

    public function user()
    {
      return $this->belongsTo(\App\User::class);
    }
}
