<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $fillable = [
        'komentar', 'user_id', 'is_admin'
    ];
}
