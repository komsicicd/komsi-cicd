<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Repo;
use Faker\Generator as Faker;

$factory->define(Repo::class, function (Faker $faker) {
    $konteks = ['Landing Page', 'WEB API', 'Frontend', 'Backend', 'Database', 'Docker'];
    $tech = ['Laravel', 'Node JS', 'React JS', 'Vue JS', 'Bootstrap'];
    return [
        'konteks' => $faker->randomElement($konteks),
        'tech' => $faker->randomElement($tech),
        'user_id' => 2,
        'dockerImage' => 'hub.docker.com'
    ];
});
