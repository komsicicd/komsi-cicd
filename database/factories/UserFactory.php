<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $pass = bcrypt('komsicicd');
    return [
        'nama_ketua' => 'admin',
        'role' => 1,
        'nama_proyek' => '',
        'nama_pembimbing' => '',
        'email' => 'admin@komsicicd.site',
        'no_telp' => 87837440698,
        'progress' => 0,
        'email_verified_at' => now(),
        'password' => $pass,
        'remember_token' => Str::random(10),
    ];
});
