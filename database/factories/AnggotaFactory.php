<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Anggota;
use Faker\Generator as Faker;

$factory->define(Anggota::class, function (Faker $faker) {
    return [
        'nama_lengkap' => $faker->name,
        'username' => $faker->userName,
        'email' => $faker->freeEmail,
        'user_id' => 2,
    ];
});
