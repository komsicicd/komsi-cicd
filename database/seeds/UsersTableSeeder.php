<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create();
        DB::table('users')->insert([
            'nama_ketua' => 'abran',
            'role' => 2,
            'nama_proyek' => 'KOMSI CICD',
            'nama_pembimbing' => 'Pak Rifqi',
            'email' => 'here.abran@gmail.com',
            'no_telp' => 8783743242323,
            'progress' => 30,
            'email_verified_at' => now(),
            'password' => bcrypt('aaaaaaaa'),
            'remember_token' => Str::random(10),
        ]);
        DB::table('users')->insert([
            'nama_ketua' => 'Tejo',
            'role' => 0,
            'nama_proyek' => 'Pembangunan Bangsa',
            'nama_pembimbing' => 'Pak Ikham Huda',
            'email' => 'tejo@gmail.com',
            'no_telp' => 87837432344535,
            'progress' => 0,
            'email_verified_at' => now(),
            'password' => bcrypt('aaaaaaaa'),
            'remember_token' => Str::random(10),
        ]);DB::table('users')->insert([
            'nama_ketua' => 'Bedjo',
            'role' => 0,
            'nama_proyek' => 'Pemberantasan Narkoba',
            'nama_pembimbing' => 'Pak Imam Faturahman',
            'email' => 'bedjo@gmail.com',
            'no_telp' => 87837432423344,
            'progress' => 0,
            'email_verified_at' => now(),
            'password' => bcrypt('aaaaaaaa'),
            'remember_token' => Str::random(10),
        ]);
    }
}
