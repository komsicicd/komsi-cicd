<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('role')->default(0);
            $table->string('nama_ketua');
            $table->string('nama_proyek');
            $table->string('nama_pembimbing');
            $table->string('email')->unique();
            $table->bigInteger('no_telp')->unique();
            $table->integer('progress')->default(0);
            $table->string('stagingLink')->nullable()->default('-');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
