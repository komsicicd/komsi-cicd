FROM php:7.2.14-apache

#install all the system dependencies and enable PHP modules
RUN apt-get update && apt-get install -y \
      libicu-dev \
      libpq-dev \
      libmcrypt-dev \
      libpng-dev \
      libfreetype6-dev \
      libjpeg62-turbo-dev \
      libgd-dev \
      libxml2-dev \
      git \
      zip \
      unzip \
      gnupg \
      libcurl4-openssl-dev \
    && rm -r /var/lib/apt/lists/* \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-configure mysqli --with-mysqli=mysqlnd \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd \
    && docker-php-ext-install zip \
    && docker-php-ext-install tokenizer \
    && docker-php-ext-install mbstring \
    && docker-php-ext-install xml 
    
RUN docker-php-ext-configure pgsql --with-pgsql=/usr/local/pgsql
RUN docker-php-ext-install pdo pgsql pdo_pgsql

RUN apt-get update && apt-get install -y libmemcached-dev \
    && pecl install memcached \
    && docker-php-ext-enable memcached.so
# RUN pecl channel-update pecl.php.net && pecl install memcached && docker-php-ext-enable memcached

#install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

#set our application folder as an environment variable
ENV APP_HOME /var/www/html

#change uid and gid of apache to docker user uid/gid
RUN usermod -u 1000 www-data && groupmod -g 1000 www-data

#change the web_root to laravel /var/www/html/public folder
RUN sed -i -e "s/html/html\/public/g" /etc/apache2/sites-enabled/000-default.conf

# enable apache module rewrite
RUN a2enmod rewrite

#copy source files and run composer
COPY --chown=1000:1000 . $APP_HOME

WORKDIR $APP_HOME

# install All Dependencies
RUN cp .env.staging .env
RUN export COMPOSER_ALLOW_SUPERUSER=1
RUN composer install --no-interaction
RUN php artisan key:generate
RUN php artisan cache:clear
# RUN php artisan migrate
# RUN php artisan db:seed
RUN php artisan config:cache
