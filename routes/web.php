<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect()->route('login');
});

// ADMIN PREFIX
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function(){
    Route::get('dashboard', 'AdminController@index')->name('dashboard.admin');

    // Proyek
    Route::get('verifyProyek', 'AdminController@verify')->name('verify.admin');
    Route::get('pProyek', 'AdminController@pp')->name('data.pp.admin');
    Route::get('Proyekt', 'AdminController@pt')->name('data.pt.admin');
    Route::post('confirmProyek', 'AdminController@confirm')->name('confirm.proyek.admin');
    Route::post('unconfirmProyek', 'AdminController@unconfirm')->name('unconfirm.proyek.admin');
    Route::post('deleteProyek', 'AdminController@delProyek')->name('del.proyek.admin');
    Route::post('dataEditProyek', 'AdminController@dataEditProyek')->name('dataEdit.proyek.admin');
    Route::post('editProyek', 'AdminController@editProyek')->name('edit.proyek.admin');
    Route::post('tambahProyek', 'AdminController@addProyek')->name('add.proyek.admin');

    // Gitlab
    Route::get('gitlab', 'AdminController@gitlab')->name('gitlab.admin');
    Route::get('gitlabDetail/{id}', 'AdminController@gitlabDetail')->name('detail.gitlab.admin');
    Route::get('dataGitlab', 'AdminController@dataGitlab')->name('data.gitlab.admin');
    Route::get('dataConfirmGitlab', 'AdminController@dataConfirmGitlab')->name('dataConfirm.gitlab.admin');
    Route::post('confirmGitlab', 'AdminController@confirmGitlab')->name('confirm.gitlab.admin');
    Route::post('unconfirmGitlab', 'AdminController@unconfirmGitlab')->name('unconfirm.gitlab.admin');

    // Gitlab Detail Anggota
    Route::get('dataGitlabDetail/{id}', 'AdminController@dataGitlabDetail')->name('data.gitlabDetail.admin');
    Route::post('tambahAnggota/{user_id}', 'AdminController@tambahAnggota')->name('add.anggota.admin');
    Route::post('daleteAnggota', 'AdminController@delAnggota')->name('del.anggota.admin');
    Route::post('dataEditAnggota/{id}', 'AdminController@dataEditAnggota')->name('dataEdit.anggota.admin');
    Route::post('editAnggota/{id}', 'AdminController@editAnggota')->name('edit.anggota.admin');

    // Gitlab Detail Repo
    Route::get('dataGitlabRepoDetail/{id}', 'AdminController@dataGitlabRepoDetail')->name('data.gitlabRepoDetail.admin');
    Route::post('tambahRepo/{user_id}', 'AdminController@tambahRepo')->name('add.repo.admin');
    Route::post('daleteRepo', 'AdminController@delRepo')->name('del.repo.admin');
    Route::post('dataEditRepo/{id}', 'AdminController@dataEditRepo')->name('dataEdit.repo.admin');
    Route::post('editRepo/{id}', 'AdminController@editRepo')->name('edit.repo.admin');
    Route::post('linkRepo/{id}', 'AdminController@linkRepo')->name('link.repo.admin');

    // Docker
    Route::get('docker', 'AdminController@docker')->name('docker.admin');
    Route::get('dataDocker', 'AdminController@dataDocker')->name('data.docker.admin');
    Route::get('dataDetailDocker', 'AdminController@dataDetailDocker')->name('detail.docker.admin');
    Route::get('dataConfirmedDocker', 'AdminController@dataConfirmedDocker')->name('dataConfirm.docker.admin');
    Route::post('confirmDocker', 'AdminController@confirmDocker')->name('confirm.docker.admin');
    Route::post('unconfirmDocker', 'AdminController@unconfirmDocker')->name('unconfirm.docker.admin');
    Route::post('komentar', 'AdminController@komentar')->name('komentar.docker.admin');

    // Info
    Route::get('info', 'AdminController@info')->name('info.admin');

});

// USER PREFIX
Route::get('verification', 'UserController@verify')->name('verify.user')->middleware('auth');
Route::group(['prefix' => 'user', 'middleware' => ['auth', 'user']], function(){
    Route::get('dashboard', 'UserController@index')->name('dashboard.user');
    Route::get('gitlab', 'UserController@gitlab')->name('gitlab.user');

    //Anggota
    Route::get('dataAnggota', 'UserController@getAnggota')->name('data.anggota.user');
    Route::post('daleteAnggota', 'UserController@delAnggota')->name('del.anggota.user');
    Route::post('tambahAnggota', 'UserController@tambahAnggota')->name('add.anggota.user');
    Route::post('dataEditAnggota', 'UserController@dataEditAnggota')->name('dataEdit.anggota.user');
    Route::post('editAnggota', 'UserController@editAnggota')->name('edit.anggota.user');
    Route::post('kunciAnggota', 'UserController@kunciAnggota')->name('kunci.anggota.user');

    //Repo
    Route::get('dataRepo', 'UserController@getRepo')->name('data.repo.user');
    Route::post('daleteRepo', 'UserController@delRepo')->name('del.repo.user');
    Route::post('tambahRepo', 'UserController@tambahRepo')->name('add.repo.user');
    Route::post('dataEditRepo', 'UserController@dataEditRepo')->name('dataEdit.repo.user');
    Route::post('editRepo', 'UserController@editRepo')->name('edit.repo.user');

    // Informasi
    Route::get('information', 'UserController@info')->name('info.user');

    // Docker
    Route::get('docker', 'UserController@docker')->name('docker.user');
    Route::post('dockerDone', 'UserController@dockerDone')->name('done.docker.user');
    Route::post('dockerAndroid', 'UserController@dockerAndroid')->name('android.docker.user');
    Route::post('dockerImage', 'UserController@dockerImage')->name('image.docker.user');
});

Auth::routes();

Route::get('/home', 'UserController@index')->name('home');

Auth::routes();
